package com.moneytransfer.web.handlers;

import com.moneytransfer.core.entities.AccountJsonDecorator;
import com.moneytransfer.core.entities.TransactionJsonDecorator;
import com.moneytransfer.core.services.AccountService;
import com.moneytransfer.core.services.ServiceFactory;
import com.moneytransfer.core.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.moneytransfer.utils.BalanceFieldFilter.zeroAmount;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionRequestHandler {
    private static Logger log = LoggerFactory.getLogger(TransactionRequestHandler.class);
    private final TransactionService transactionService = ServiceFactory.INSTANCE.getTransactionService();
    private final AccountService accountService = ServiceFactory.INSTANCE.getAccountService();

    @GET
    @Path("/{transactionId}")
    public TransactionJsonDecorator getTransactionById(@PathParam("transactionId") Long transactionId) {
        if (log.isDebugEnabled())
            log.debug("Request Received for get Transaction by Id " + transactionId);
        final Optional<TransactionJsonDecorator> transaction = transactionService.getTransaction(transactionId);
        if (!transaction.isPresent()) {
            throw new WebApplicationException("Transaction with id - " + transactionId + " Not Found",
                    Response.Status.NOT_FOUND);
        }
        return transaction.get();
    }

    @GET
    @Path("/all")
    public List<TransactionJsonDecorator> getAllTransactions() {
        if (log.isDebugEnabled())
            log.debug("Request Received for get all Transactions ");
        return transactionService.getAllTransactions();
    }

    @POST
    @Path("/create")
    public Response createTransaction(TransactionJsonDecorator transactionJsonDecorator) {
        if (log.isDebugEnabled())
            log.debug("Request Received for create Transaction " + transactionJsonDecorator);
        final BigDecimal amount = transactionJsonDecorator.getAmount();
        if (amount.compareTo(zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        final Optional<AccountJsonDecorator> senderAccount = accountService.getAccount(transactionJsonDecorator.getSenderId());
        if (!senderAccount.isPresent()) {
            throw new WebApplicationException("Account with id - " + transactionJsonDecorator.getSenderId() + " Not Found",
                    Response.Status.NOT_FOUND);
        }
        final Optional<AccountJsonDecorator> receiverAccount = accountService.getAccount(transactionJsonDecorator.getReceiverId());
        if (!receiverAccount.isPresent()) {
            throw new WebApplicationException("Account with id - " + transactionJsonDecorator.getReceiverId() + " Not Found",
                    Response.Status.NOT_FOUND);
        }
        transactionService.performTransaction(senderAccount.get(), receiverAccount.get(), amount);
        return Response.status(Response.Status.OK).build();
    }

}
