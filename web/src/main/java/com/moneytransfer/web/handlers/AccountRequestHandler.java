package com.moneytransfer.web.handlers;

import com.moneytransfer.core.entities.AccountJsonDecorator;
import com.moneytransfer.core.services.AccountService;
import com.moneytransfer.core.services.ServiceFactory;
import com.moneytransfer.core.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.moneytransfer.utils.BalanceFieldFilter.zeroAmount;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountRequestHandler {
    private static final Logger log = LoggerFactory.getLogger(AccountRequestHandler.class);
    private AccountService accountService = ServiceFactory.INSTANCE.getAccountService();
    private TransactionService transactionService = ServiceFactory.INSTANCE.getTransactionService();

    @GET
    @Path("/{accountId}")
    public AccountJsonDecorator getAccount(@PathParam("accountId") long accountId) {
        return getAccountJsonDecorator(accountId);
    }

    @POST
    @Path("/create")
    public AccountJsonDecorator createAccount(AccountJsonDecorator account) {
        final Long newAccountId = accountService.createAccount(account);
        return getAccountJsonDecorator(newAccountId);
    }

    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") long accountId) {
        final boolean deleteAccount = accountService.deleteAccount(accountId);
        if (deleteAccount) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/all")
    public List<AccountJsonDecorator> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @GET
    @Path("/{accountId}/balance")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) {
        final AccountJsonDecorator accountJsonDecorator = getAccountJsonDecorator(accountId);
        return accountJsonDecorator.getBalance();
    }

    @PUT
    @Path("/{accountId}/deposit/{amount}")
    public AccountJsonDecorator deposit(@PathParam("accountId") long accountId, @PathParam("amount") BigDecimal amount) {
        if (amount.compareTo(zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        if (log.isDebugEnabled())
            log.debug("Deposit " + amount + " Account ID = " + accountId);
        accountService.updateAccountBalance(accountId, amount);
        return getAccountJsonDecorator(accountId);
    }

    @PUT
    @Path("/{accountId}/withdraw/{amount}")
    public AccountJsonDecorator withdraw(@PathParam("accountId") long accountId, @PathParam("amount") BigDecimal amount) {
        if (amount.compareTo(zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }
        BigDecimal delta = amount.negate();
        if (log.isDebugEnabled())
            log.debug("Withdraw " + delta + " Account ID = " + accountId);
        accountService.updateAccountBalance(accountId, delta);
        return getAccountJsonDecorator(accountId);
    }

    @PUT
    @Path("/{accountId}/send/{receiverId},{amount}")
    public AccountJsonDecorator send(@PathParam("accountId") long accountId, @PathParam("receiverId") long receiverId,
                                     @PathParam("amount") BigDecimal amount) {
        if (amount.compareTo(zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }

        final AccountJsonDecorator senderAccount = getAccountJsonDecorator(accountId);
        final AccountJsonDecorator receiverAccount = getAccountJsonDecorator(receiverId);
        transactionService.performTransaction(senderAccount, receiverAccount, amount);
        return getAccountJsonDecorator(accountId);
    }

    private AccountJsonDecorator getAccountJsonDecorator(long accountId) {
        final Optional<AccountJsonDecorator> account = accountService.getAccount(accountId);
        if (account.isPresent()) {
            return account.get();
        } else {
            throw new WebApplicationException("Can't find account with id - " + accountId, Response.Status.NOT_FOUND);
        }
    }
}
