package com.moneytransfer.web.handlers;

import com.moneytransfer.core.entities.ClientJsonDecorator;
import com.moneytransfer.core.services.ClientService;
import com.moneytransfer.core.services.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/client")
@Produces(MediaType.APPLICATION_JSON)
public class ClientRequestHandler {
    private ClientService clientService = ServiceFactory.INSTANCE.getClientService();
    private static Logger log = LoggerFactory.getLogger(ClientRequestHandler.class);


    @GET
    @Path("/{clientId}")
    public ClientJsonDecorator getClientById(@PathParam("clientId") Long clientId) {
        if (log.isDebugEnabled())
            log.debug("Request Received for get Client by Id " + clientId);
        final Optional<ClientJsonDecorator> clientById = clientService.getClient(clientId);
        if (!clientById.isPresent()) {
            throw new WebApplicationException("Client Not Found", Response.Status.NOT_FOUND);
        }
        return clientById.get();
    }

    @GET
    @Path("/name/{clientName}")
    public ClientJsonDecorator getClientByName(@PathParam("clientName") String clientName) {
        if (log.isDebugEnabled())
            log.debug("Request Received for get Client by Name " + clientName);
        final Optional<ClientJsonDecorator> clientByName = clientService.getClientByName(clientName);
        if (!clientByName.isPresent()) {
            throw new WebApplicationException("Client Not Found", Response.Status.NOT_FOUND);
        }
        return clientByName.get();
    }

    @GET
    @Path("/all")
    public List<ClientJsonDecorator> getAllClients() {
        if (log.isDebugEnabled())
            log.debug("Request Received for get All Clients");
        return clientService.getAllClients();
    }

    @POST
    @Path("/create")
    public ClientJsonDecorator createClient(ClientJsonDecorator client) {
        if (log.isDebugEnabled())
            log.debug("Request Received for create Client " + client);
        final Long newClientId = clientService.createClient(client);
        final Optional<ClientJsonDecorator> newClient = clientService.getClient(newClientId);
        if (newClient.isPresent()) {
            return newClient.get();
        } else {
            throw new WebApplicationException("Can't create a client", Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @PUT
    @Path("/{clientId}")
    public Response updateClient(@PathParam("clientId") long clientId, ClientJsonDecorator client) {
        if (log.isDebugEnabled())
            log.debug("Request Received for get Client by Id " + clientId);
        final Optional<ClientJsonDecorator> clientById = clientService.getClient(clientId);
        if (!clientById.isPresent()) {
            throw new WebApplicationException("Client Not Found", Response.Status.NOT_FOUND);
        }
        clientService.updateClient(clientById.get().getClientId(), client);
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    @Path("/{clientId}")
    public Response deleteClient(@PathParam("clientId") long clientId) {
        if (log.isDebugEnabled())
            log.debug("Request Received for delete Client " + clientId);
        final boolean deleteClient = clientService.deleteClient(clientId);
        if (deleteClient) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
