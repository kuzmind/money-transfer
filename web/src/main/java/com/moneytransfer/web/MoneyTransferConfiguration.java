package com.moneytransfer.web;

import com.moneytransfer.utils.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoneyTransferConfiguration {
    private static final Logger log = LoggerFactory.getLogger(MoneyTransferConfiguration.class);
    private final int httpPort;
    private final int DEFAULT_HTTP_PORT = 8080;
    private final String HTTP_PORT_PROPERTY = "http.port";

    public MoneyTransferConfiguration(PropertyReader propertyReader) {
        httpPort = propertyReader.getIntegerProperty(HTTP_PORT_PROPERTY, DEFAULT_HTTP_PORT);
        log.debug("Found property " + HTTP_PORT_PROPERTY + " = " + httpPort);
    }

    public int getHttpPort() {
        return httpPort;
    }
}
