package com.moneytransfer.web.jersey;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

/**
 * Jersey money transfer configuration. Initializes handlers from specific package and
 * allows to present application exceptions with fixed error response json scheme.
 */
public class MoneyTransferApp extends ResourceConfig {
    public MoneyTransferApp() {
        packages("com.moneytransfer.web.handlers");
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        register(ConstraintViolationExceptionMapper.class);
        register(GenericExceptionMapper.class);
    }
}
