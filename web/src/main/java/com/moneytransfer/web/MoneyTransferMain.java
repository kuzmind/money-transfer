package com.moneytransfer.web;

import com.moneytransfer.utils.PropertyReader;
import com.moneytransfer.web.jersey.MoneyTransferApp;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.util.logging.LogManager;

public class MoneyTransferMain {
    public static void main(String[] args) throws Exception {
        PropertyReader propertyReader = new PropertyReader();
        MoneyTransferConfiguration configuration = new MoneyTransferConfiguration(propertyReader);
        initLogging();
        ServletContextHandler servletContextHandler = createServletContextHandler();
        runJettyServer(servletContextHandler, configuration.getHttpPort());
    }

    private static void runJettyServer(ServletContextHandler servletContextHandler, int httpPort) throws Exception {
        Server server = new Server(httpPort);
        try {
            server.setHandler(servletContextHandler);
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }

    private static ServletContextHandler createServletContextHandler() {
        ServletContainer servlet = new ServletContainer(new MoneyTransferApp());
        ServletHolder servletHolder = new ServletHolder(servlet);

        ServletContextHandler servletContextHandler = new ServletContextHandler();
        servletContextHandler.setContextPath("/");
        servletContextHandler.addServlet(servletHolder, "/*");

        servletContextHandler.addEventListener(new ContextLoaderListener());
        servletContextHandler.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
        return servletContextHandler;
    }

    private static void initLogging() {
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();
    }
}
