package com.moneytransfer.web.response;

public abstract class SimpleResponse {
    private final boolean success;

    public SimpleResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
