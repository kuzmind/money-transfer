package com.moneytransfer.web.response;

public class OkResponse extends SimpleResponse {
    public OkResponse() {
        super(true);
    }
}
