package com.moneytransfer.web.handlers;

import com.moneytransfer.core.entities.AccountJsonDecorator;
import com.moneytransfer.core.entities.ClientJsonDecorator;
import com.moneytransfer.core.entities.TransactionJsonDecorator;
import com.moneytransfer.web.ServletContainerCreatingTest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;

public class TransactionHandlerTest extends ServletContainerCreatingTest {

    /**
     * Create transaction between accountId = 1 and accountId = 2
     * account 1 before transaction - ('test1', 150.50, 'USD');
     * account 2 before transaction - ('test2', 200.00, 'USD');
     * amount = 50
     *
     * Expected:
     * account 1 after transaction - ('test1', 100.50, 'USD');
     * account 2 after transaction - ('test2', 250.00, 'USD');
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void createTransactionTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/transaction/create").build();
        long senderId = 1L;
        long receiverId = 2L;
        final BigDecimal amount = new BigDecimal(50);
        TransactionJsonDecorator transaction = new TransactionJsonDecorator(senderId, receiverId, amount);
        String jsonInString = mapper.writeValueAsString(transaction);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = getHttpPost(uri);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);

        uri = builder.setPath("/transaction/1").build();
        HttpGet getRequest = new HttpGet(uri);
        response = httpClient.execute(getRequest);
        statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);

        final AccountJsonDecorator sender = AccountHandlerTest.generateHttpRequestForAccount("/account/1");
        Assert.assertNotNull(sender);
        Assert.assertEquals("test1", sender.getClientName());
        Assert.assertEquals("USD", sender.getCurrencyCode());
        Assert.assertEquals(new BigDecimal(100.50).setScale(2, RoundingMode.HALF_EVEN), sender.getBalance());
        final AccountJsonDecorator receiver = AccountHandlerTest.generateHttpRequestForAccount("/account/2");
        Assert.assertNotNull(receiver);
        Assert.assertEquals("test2", receiver.getClientName());
        Assert.assertEquals("USD", receiver.getCurrencyCode());
        Assert.assertEquals(new BigDecimal(250.00).setScale(2, RoundingMode.HALF_EVEN), receiver.getBalance());
    }
}
