package com.moneytransfer.web.handlers;

import com.moneytransfer.core.entities.AccountJsonDecorator;
import com.moneytransfer.web.ServletContainerCreatingTest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;

public class AccountHandlerTest extends ServletContainerCreatingTest {

    /**
     * Check if account with id = 1 exist
     * Fields :('test1', 150.5000, 'USD');
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void getAccountById() throws IOException, URISyntaxException {
        AccountJsonDecorator account = generateHttpRequestForAccount("/account/1");
        Assert.assertNotNull(account);
        Assert.assertEquals("test1", account.getClientName());
        Assert.assertEquals("USD", account.getCurrencyCode());
        Assert.assertEquals(new BigDecimal(150.50).setScale(2, RoundingMode.HALF_EVEN), account.getBalance());
    }

    /**
     * Create account and check all fields
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void createAccountTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/create").build();
        BigDecimal newBalance = new BigDecimal(1000).setScale(2, RoundingMode.HALF_EVEN);
        String newClientName = "createAccountTest";
        String newCurrencyCode = "RUB";
        AccountJsonDecorator newAccount = new AccountJsonDecorator(newClientName, newBalance, newCurrencyCode);
        String jsonInString = mapper.writeValueAsString(newAccount);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = getHttpPost(uri);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        AccountJsonDecorator accountFromHttp = retrieveAccountInstanceFromHttp(response);
        Assert.assertEquals(newClientName, accountFromHttp.getClientName());
        Assert.assertEquals(newCurrencyCode, accountFromHttp.getCurrencyCode());
        Assert.assertEquals(newBalance, accountFromHttp.getBalance());
    }

    /**
     * Delete account with id = 1 and check if response = OK
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void deleteAccountTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/1").build();
        HttpDelete request = getHttpDelete(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
    }

    /**
     * Try to delete not existing account
     * Expected : status 404
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void deleteAccountNegativeTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/200").build();
        HttpDelete request = getHttpDelete(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_NOT_FOUND, statusCode);
    }

    /**
     * Get all accounts and check if they are not null
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void getAllAccountsTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/all").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        String jsonString = EntityUtils.toString(response.getEntity());
        AccountJsonDecorator[] accounts = mapper.readValue(jsonString, AccountJsonDecorator[].class);
        Assert.assertTrue(accounts.length > 0);
    }

    /**
     * Get account balance from AccountId = 6
     * Account fields :('test2', 500.00, 'GBP');
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void getAccountBalanceTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/6/balance").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        String balance = EntityUtils.toString(response.getEntity());
        BigDecimal balanceFromHttp = new BigDecimal(balance).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal balanceFromSQLFile = new BigDecimal(500).setScale(2, RoundingMode.HALF_EVEN);
        Assert.assertEquals(balanceFromHttp, balanceFromSQLFile);
    }

    /**
     * Withdraw from accountId = 2 100
     * Account fields: ('test2', 200.00, 'USD');
     * Expected : balance = 100, Status = 200
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void withdrawFromAccountTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/2/withdraw/100").build();
        HttpPut request = new HttpPut(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        AccountJsonDecorator accountFromHttp = retrieveAccountInstanceFromHttp(response);
        BigDecimal balanceAfterWithdraw = new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN);
        Assert.assertEquals(balanceAfterWithdraw, accountFromHttp.getBalance());
    }

    /**
     * Withdraw from accountId = 2 1000
     * Account fields: ('test2', 100.00, 'USD');
     * Expected : Status = 404
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void withdrawFromAccountNegativeTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/2/withdraw/1000").build();
        HttpPut request = new HttpPut(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_INTERNAL_SERVER_ERROR, statusCode);
    }

    /**
     * Deposit on accountId = 3 100
     * Account fields: ('test1', 500.00, 'EUR');
     * Expected : balance = 600, Status = 200
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void depositOnAccountTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/3/deposit/100").build();
        HttpPut request = new HttpPut(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        AccountJsonDecorator accountFromHttp = retrieveAccountInstanceFromHttp(response);
        BigDecimal balanceAfterWithdraw = new BigDecimal(600).setScale(2, RoundingMode.HALF_EVEN);
        Assert.assertEquals(balanceAfterWithdraw, accountFromHttp.getBalance());
    }

    /**
     * Deposit on accountId = 3 -1000
     * Account fields: ('test1', 600.00, 'EUR');
     * Expected : Status = 404
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void depositOnAccountNegativeTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/3/deposit/-1000").build();
        HttpPut request = new HttpPut(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_BAD_REQUEST, statusCode);
    }

    /**
     * Send 100 from accountId = 4 to accountId = 5
     * VALUES ('test2', 500.00, 'EUR');
     * VALUES ('test1', 500.00, 'GBP');
     * @see com.moneytransfer.utils.MoneyExchanger
     * Expected:
     *           Transaction fee because of diff currencies = 2
     *           100 UER = 89.6 GBP
     *           accountId = 4 values : ('test2', 398.00, 'EUR')
     *           accountId = 5 values : ('test1', 589.60, 'GBP')
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void sendMoneyBetweenAccountsTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/account/4/send/5,100").build();
        HttpPut request = new HttpPut(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        AccountJsonDecorator accountFromHttp = retrieveAccountInstanceFromHttp(response);
        BigDecimal balanceAfterWithdraw = new BigDecimal(398).setScale(2, RoundingMode.HALF_EVEN);
        Assert.assertEquals(balanceAfterWithdraw, accountFromHttp.getBalance());

        uri = builder.setPath("/account/5").build();
        HttpGet getRequest = new HttpGet(uri);
        response = httpClient.execute(getRequest);
        statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        AccountJsonDecorator account = retrieveAccountInstanceFromHttp(response);
        Assert.assertNotNull(account);
        Assert.assertEquals(new BigDecimal(589.60).setScale(2, RoundingMode.HALF_EVEN), account.getBalance());
    }

    public static AccountJsonDecorator generateHttpRequestForAccount(String path) throws URISyntaxException, IOException {
        URI uri = builder.setPath(path).build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);

        return retrieveAccountInstanceFromHttp(response);
    }

    private static AccountJsonDecorator retrieveAccountInstanceFromHttp(HttpResponse response) throws IOException {
        String jsonString = EntityUtils.toString(response.getEntity());
        return mapper.readValue(jsonString, AccountJsonDecorator.class);
    }
}
