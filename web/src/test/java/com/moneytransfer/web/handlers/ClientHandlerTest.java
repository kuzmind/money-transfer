package com.moneytransfer.web.handlers;

import com.moneytransfer.core.entities.ClientJsonDecorator;
import com.moneytransfer.web.ServletContainerCreatingTest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class ClientHandlerTest extends ServletContainerCreatingTest {

    /**
     * Get client by id = 1
     * expected: OK respond
     * VALUES ('test1', 'test1@gmail.com');
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void getClientByIdTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/1").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        ClientJsonDecorator account = retrieveClientInstanceFromHttp(response);
        Assert.assertNotNull(account);
        String clientNameFromScript = "test1";
        String clientEmailFromScript = "test1@gmail.com";
        Assert.assertEquals(clientNameFromScript, account.getClientName());
        Assert.assertEquals(clientEmailFromScript, account.getClientEmail());
    }

    /**
     * Get client by name = test1
     * expected: OK respond
     * VALUES ('test1', 'test1@gmail.com');
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void getClientByNameTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/name/test1").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        ClientJsonDecorator account = retrieveClientInstanceFromHttp(response);
        Assert.assertNotNull(account);
        String clientNameFromScript = "test1";
        String clientEmailFromScript = "test1@gmail.com";
        Assert.assertEquals(clientNameFromScript, account.getClientName());
        Assert.assertEquals(clientEmailFromScript, account.getClientEmail());
    }

    /**
     * Get all clients and check if they are exist
     * expected: receiving array > 0
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void getAllClientsTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/all").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        String jsonString = EntityUtils.toString(response.getEntity());
        ClientJsonDecorator[] clients = mapper.readValue(jsonString, ClientJsonDecorator[].class);
        Assert.assertTrue(clients.length > 0);
    }

    /**
     * Delete client with id = 3 and check if response = OK
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void deleteClientTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/4").build();
        HttpDelete request = getHttpDelete(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
    }

    /**
     * Try to delete not existing client
     * Expected : status 404
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void deleteClientNegativeTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/200").build();
        HttpDelete request = getHttpDelete(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_NOT_FOUND, statusCode);
    }

    /**
     * Update client with id = 3 and check
     * new data : clientName = newName ; clientEmail = newEmail@mail.com
     * Expected : status OK
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void updateClientTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/3").build();
        String newClientName = "updateClientTest";
        String newClientEmail = "updateClientTest@mail.com";
        ClientJsonDecorator newClient = new ClientJsonDecorator(newClientName, newClientEmail);
        String jsonInString = mapper.writeValueAsString(newClient);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPut request = getHttpPut(uri);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
    }

    /**
     * Create client and check all fields
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void createClientTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/create").build();
        String newClientName = "createClientTest";
        String newClientEmail = "createClientTest@mail.com";
        ClientJsonDecorator newClient = new ClientJsonDecorator(newClientName, newClientEmail);
        String jsonInString = mapper.writeValueAsString(newClient);
        StringEntity entity = new StringEntity(jsonInString);
        HttpPost request = getHttpPost(uri);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
        ClientJsonDecorator clientFromHttp = retrieveClientInstanceFromHttp(response);
        Assert.assertEquals(newClientName, clientFromHttp.getClientName());
        Assert.assertEquals(newClientEmail, clientFromHttp.getClientEmail());
    }

    private ClientJsonDecorator retrieveClientInstanceFromHttp(HttpResponse response) throws IOException {
        String jsonString = EntityUtils.toString(response.getEntity());
        return mapper.readValue(jsonString, ClientJsonDecorator.class);
    }
}
