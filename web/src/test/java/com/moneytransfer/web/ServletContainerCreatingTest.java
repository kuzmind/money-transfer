package com.moneytransfer.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneytransfer.utils.PropertyReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class ServletContainerCreatingTest {
    protected static final int RESPOND_OK = 200;
    protected static final int RESPOND_NOT_FOUND = 404;
    protected static final int RESPOND_INTERNAL_SERVER_ERROR = 500;
    protected static final int RESPOND_BAD_REQUEST = 400;
    private static final int WAITING_TIME = 5000;
    protected static CloseableHttpClient httpClient;
    protected static PoolingHttpClientConnectionManager connManager;
    protected static URIBuilder builder;
    protected static ObjectMapper mapper;

    /**
     * Start container on background and wait 10 for fully initialization. Not the best solution but for test is ok.
     *
     * @see MoneyTransferMain
     */
    @BeforeClass
    public static void creationContainer() {
//        initConnectionEntities();
        try {
            //we need to start server
            Thread containerLauncherThread = new Thread(() -> {
                try {
                    MoneyTransferMain.main(new String[]{});
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e.getMessage());
                }
            });
            containerLauncherThread.setDaemon(true);
            containerLauncherThread.start();
            //it because in MoneyTransferMain.main we have server.join() method
            Thread.sleep(WAITING_TIME);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Before
    public void initConnectionEntities() {
        MoneyTransferConfiguration configuration = new MoneyTransferConfiguration(new PropertyReader());
        connManager = new PoolingHttpClientConnectionManager();
        String host = "localhost:" + configuration.getHttpPort();
        builder = new URIBuilder().setScheme("http").setHost(host);
        httpClient = HttpClients.custom().setConnectionManager(connManager).setConnectionManagerShared(true).build();
        mapper = new ObjectMapper();
    }

    @AfterClass
    public static void closeClient() {
        HttpClientUtils.closeQuietly(httpClient);
    }

    @Test
    public void creationAndGetByIdTest() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/client/1").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(RESPOND_OK, statusCode);
    }

    public HttpPut getHttpPut(URI uri) {
        HttpPut request = new HttpPut(uri);
        request.setHeader("Content-type", "application/json");
        return request;
    }

    public HttpDelete getHttpDelete(URI uri) {
        HttpDelete request = new HttpDelete(uri);
        request.setHeader("Content-type", "application/json");
        return request;
    }

    public HttpPost getHttpPost(URI uri) {
        HttpPost request = new HttpPost(uri);
        request.setHeader("Content-type", "application/json");
        return request;
    }
}
