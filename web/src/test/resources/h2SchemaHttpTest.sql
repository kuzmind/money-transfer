DROP TABLE IF EXISTS Client;

CREATE TABLE Client
(
    ClientId    LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    ClientName  VARCHAR(40)                     NOT NULL,
    ClientEmail VARCHAR(40)                     NOT NULL,
);

CREATE UNIQUE INDEX idx_clt on Client (ClientName, ClientEmail);

INSERT INTO Client (ClientName, ClientEmail)
VALUES ('test1', 'test1@gmail.com');
INSERT INTO Client (ClientName, ClientEmail)
VALUES ('test2', 'test2@gmail.com');
INSERT INTO Client (ClientName, ClientEmail)
VALUES ('test3', 'test3@gmail.com');
INSERT INTO Client (ClientName, ClientEmail)
VALUES ('test4', 'test4@gmail.com');

DROP TABLE IF EXISTS Account;

CREATE TABLE Account
(
    AccountId    LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    ClientName   VARCHAR(40),
    Balance      DECIMAL(19, 2),
    CurrencyCode VARCHAR(3)
);

INSERT INTO Account (ClientName, Balance, CurrencyCode)
VALUES ('test1', 150.50, 'USD');
INSERT INTO Account (ClientName, Balance, CurrencyCode)
VALUES ('test2', 200.00, 'USD');
INSERT INTO Account (ClientName, Balance, CurrencyCode)
VALUES ('test1', 500.00, 'EUR');
INSERT INTO Account (ClientName, Balance, CurrencyCode)
VALUES ('test2', 500.00, 'EUR');
INSERT INTO Account (ClientName, Balance, CurrencyCode)
VALUES ('test1', 500.00, 'GBP');
INSERT INTO Account (ClientName, Balance, CurrencyCode)
VALUES ('test2', 500.00, 'GBP');

DROP TABLE IF EXISTS Transactions;

CREATE TABLE Transactions
(
    TransactionId           LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    SenderId                LONG                            NOT NULL,
    ReceiverId              LONG                            NOT NULL,
    Amount                  DECIMAL(19, 2),
    CurrencyCode            VARCHAR(3),
    TransferFee             DECIMAL(19, 2),
    TransferringCoefficient DECIMAL(19, 2)
);
