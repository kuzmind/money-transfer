Money Transfer
============

Money transfer application for sending money between accounts.

## Requirements:

- java 8 and maven 3
- running Jetty on http://localhost:8080 (configurable)
- running H2 on localhost with default configurations
## Offers:

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| GET | /client/{clientId} | get client by id | 
| GET | /client/{clientName} | get client by client name | 
| GET | /client/all | get all clients | 
| POST | /client/create | create a new client | 
| PUT | /client/{clientId} | update client | 
| DELETE | /client/{clientId} | remove client | 
| GET | /account/{accountId} | get account by accountId | 
| POST | /account/create | create a new account
| GET | /account/all | get all accounts | 
| DELETE | /account/{accountId} | remove account by accountId | 
| GET | /account/{accountId}/balance | get account balance by accountId | 
| PUT | /account/{accountId}/withdraw/{amount} | withdraw money from account | 
| PUT | /account/{accountId}/deposit/{amount} | deposit money to account | 
| PUT | /account/{accountId}/send/{receiverId},{amount} | send money from accountId to receiverId | 
| GET | /transaction/{transactionId} | get transaction by id | 
| GET | /transaction/all | get all transactions | 
| POST | /transaction | perform transaction between two accounts | 

Examples of simple request see below.

## How to use:

Examples:

request:
curl -X POST localhost:8080/account/create -H 'Content-type:application/json' -d '{"clientName": "Samwise Gamgee", "balance": "1000", "currencyCode":"USD"}'
respond:
{"accountId":1,"clientName":"Samwise Gamgee","balance":1000.00,"currencyCode":"USD"}
------------
request:
curl -X PUT localhost:8080/account/1/send/2,500 -H 'Content-type:application/json' -v
respond:
{"accountId":1,"clientName":"Samwise Gamgee","balance":500.00,"currencyCode":"USD"}
------------------
request:
curl -X GET localhost:8080/transaction/1/ -H 'Content-type:application/json' -v
respond:
{"transactionId":1,"senderId":1,"receiverId":2,"amount":500.00,"currencyCode":"USD","transferFee":0.00,"transferringCoefficient":1.00}

## How to start application:

- run mvn exec:java in web-service folder
- or explicitly start com.moneytransfer.web.MoneyTransferMain.main()
