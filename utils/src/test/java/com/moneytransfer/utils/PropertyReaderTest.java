package com.moneytransfer.utils;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;

public class PropertyReaderTest {
    private static final String TEST_FILE_NAME = "test.properties";
    private static PropertyReader propertyReader;

    @BeforeClass
    public static void initPropertyReader() {
        propertyReader = new PropertyReader(TEST_FILE_NAME);
    }

    @Test
    public void propertyReaderInitTest() {
        Properties properties = propertyReader.getProperties();
        Assert.assertNotNull(properties);
        int size = properties.size();
        Assert.assertEquals(3, size);
    }

    @Test
    public void stringPropertyTest() {
        String test1Value = propertyReader.getStringProperty("test1");
        String test2Value = propertyReader.getStringProperty("test2");
        String test3Value = propertyReader.getStringProperty("test3");
        Assert.assertEquals("1", test1Value);
        Assert.assertEquals("2", test2Value);
        Assert.assertEquals("3", test3Value);
    }

    @Test
    public void stringDefaultPropertyTest() {
        String aDefault = "default";
        String defaultValue = propertyReader.getStringProperty("asd", aDefault);
        Assert.assertEquals(aDefault, defaultValue);
    }

    @Test
    public void stringPropertyNegativeTest() {
        String fff = propertyReader.getStringProperty("fff");
        Assert.assertNull(fff);
    }

    @Test
    public void integerDefaultPropertyTest() {
        int defaultResult = propertyReader.getIntegerProperty("asd", 3);
        Assert.assertEquals(3, defaultResult);
    }
}
