package com.moneytransfer.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BalanceFieldFilter {
    public static final BigDecimal zeroAmount = new BigDecimal(0).setScale(2);
    public static BigDecimal scaleAndRound(BigDecimal object) {
        return object.setScale(2, RoundingMode.HALF_EVEN);
    }
}
