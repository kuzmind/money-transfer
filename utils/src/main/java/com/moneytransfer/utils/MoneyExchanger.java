package com.moneytransfer.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * Example class for currency exchange
 * Algorithm based on translation currency to USD. Back and forth
 */
public class MoneyExchanger {
    private static final Map<String, BigDecimal> rates = new HashMap<>();
    private static final BigDecimal bankFee = new BigDecimal(0.02);
    static {
        rates.put("USD", new BigDecimal(1));
        rates.put("RUB", new BigDecimal(0.016));
        rates.put("PLN", new BigDecimal(0.26));
        rates.put("EUR", new BigDecimal(1.12));
        rates.put("GBP", new BigDecimal(1.25));
    }

    public static BigDecimal getTransactionAmount(BigDecimal amount, String senderCurrencyCode, String receiverCurrencyCode) {
        final BigDecimal senderCoefficient = rates.get(senderCurrencyCode.toUpperCase());
        if (senderCoefficient == null) {
            throw new UnsupportedOperationException("Can't perform calculation. We don't support currency - " + senderCurrencyCode);
        }
        final BigDecimal senderAmountBasedOnUSD = amount.multiply(senderCoefficient);
        final BigDecimal receiverCoefficient = rates.get(receiverCurrencyCode.toUpperCase());
        if (receiverCoefficient == null) {
            throw new UnsupportedOperationException("Can't perform calculation. We don't support currency - " + receiverCurrencyCode);
        }
        final BigDecimal receiverAmountBasedOnUSD = senderAmountBasedOnUSD.divide(receiverCoefficient, RoundingMode.HALF_EVEN);
        return receiverAmountBasedOnUSD;
    }

    public static BigDecimal getTransactionCoefficient(BigDecimal amount, String senderCurrencyCode, String receiverCurrencyCode) {
        final BigDecimal senderCoefficient = rates.get(senderCurrencyCode.toUpperCase());
        if (senderCoefficient == null) {
            throw new UnsupportedOperationException("Can't perform calculation. We don't support currency - " + senderCurrencyCode);
        }
        final BigDecimal receiverCoefficient = rates.get(receiverCurrencyCode.toUpperCase());
        if (receiverCoefficient == null) {
            throw new UnsupportedOperationException("Can't perform calculation. We don't support currency - " + receiverCurrencyCode);
        }
        final BigDecimal receiverAmountBasedOnUSD = senderCoefficient.divide(receiverCoefficient, RoundingMode.HALF_EVEN);
        return receiverAmountBasedOnUSD;
    }

    public static BigDecimal getBankFee() {
        return bankFee;
    }
}
