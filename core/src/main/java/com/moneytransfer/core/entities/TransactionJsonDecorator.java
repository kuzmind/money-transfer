package com.moneytransfer.core.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moneytransfer.datastorage.entities.Transaction;

import java.math.BigDecimal;

public class TransactionJsonDecorator {
    @JsonProperty(required = true)
    private long transactionId;
    @JsonProperty(required = true)
    private long senderId;
    @JsonProperty(required = true)
    private long receiverId;
    @JsonProperty(required = true)
    private BigDecimal amount;
    @JsonProperty(required = true)
    private String currencyCode;
    @JsonProperty(required = true)
    private BigDecimal transferFee;
    @JsonProperty(required = true)
    private BigDecimal transferringCoefficient;

    public TransactionJsonDecorator() {}

    public TransactionJsonDecorator(long senderId, long receiverId, BigDecimal amount, String currencyCode) {
        this(senderId, receiverId, amount);
        this.currencyCode = currencyCode;
    }

    public TransactionJsonDecorator(long senderId, long receiverId, BigDecimal amount) {
        this(senderId, receiverId);
        this.amount = amount;
    }

    private TransactionJsonDecorator(long senderId, long receiverId) {
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public TransactionJsonDecorator(Transaction transaction) {
        this.transactionId = transaction.getTransactionId();
        this.senderId = transaction.getSenderId();
        this.receiverId = transaction.getReceiverId();
        this.amount = transaction.getAmount();
        this.currencyCode = transaction.getCurrencyCode();
        this.transferFee = transaction.getTransferFee();
        this.transferringCoefficient = transaction.getTransferringCoefficient();
    }

    public long getTransactionId() {
        return transactionId;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getTransferFee() {
        return transferFee;
    }

    public BigDecimal getTransferringCoefficient() {
        return transferringCoefficient;
    }
}
