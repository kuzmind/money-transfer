package com.moneytransfer.core.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moneytransfer.datastorage.entities.Client;

public class ClientJsonDecorator {
    @JsonProperty(required = true)
    private long clientId;

    @JsonProperty(required = true)
    private String clientName;

    @JsonProperty(required = true)
    private String clientEmail;

    public ClientJsonDecorator() {}

    public ClientJsonDecorator(String clientName, String clientEmail) {
        this.clientName = clientName;
        this.clientEmail = clientEmail;
    }

    public ClientJsonDecorator(Client client) {
        this.clientEmail = client.getClientEmail();
        this.clientId = client.getClientId();
        this.clientName = client.getClientName();
    }

    public long getClientId() {
        return clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }
}
