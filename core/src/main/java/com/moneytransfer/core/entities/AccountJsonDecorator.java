package com.moneytransfer.core.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moneytransfer.datastorage.entities.Account;

import java.math.BigDecimal;

public class AccountJsonDecorator {

    @JsonProperty(required = true)
    private long accountId;

    @JsonProperty(required = true)
    private String clientName;

    @JsonProperty(required = true)
    private BigDecimal balance;

    @JsonProperty(required = true)
    private String currencyCode;

    public AccountJsonDecorator() {}

    public AccountJsonDecorator(Account account) {
        this.accountId = account.getAccountId();
        this.balance = account.getBalance();
        this.clientName = account.getClientName();
        this.currencyCode = account.getCurrencyCode();
    }

    public AccountJsonDecorator(String clientName, BigDecimal balance, String currencyCode) {
        this.clientName = clientName;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getClientName() {
        return clientName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}
