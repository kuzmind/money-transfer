package com.moneytransfer.core.config;

import com.moneytransfer.utils.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseCoreConfiguration {
    private static final Logger log = LoggerFactory.getLogger(BaseCoreConfiguration.class);
    protected final String DAOMode;

    public BaseCoreConfiguration(PropertyReader propertyReader) {
        final String daoModeKey = "dao.mode";
        this.DAOMode = propertyReader.getStringProperty(daoModeKey, getDefaultDAOMode());
        log.debug("Found property " + daoModeKey + " = " + DAOMode);
    }

    public static String getDefaultDAOMode() {
        return "h2";
    }

    public String getDAOMode() {
        return DAOMode;
    }
}
