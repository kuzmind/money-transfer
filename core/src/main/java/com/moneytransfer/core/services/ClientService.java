package com.moneytransfer.core.services;

import com.moneytransfer.core.config.BaseCoreConfiguration;
import com.moneytransfer.core.entities.ClientJsonDecorator;
import com.moneytransfer.datastorage.dao.api.ClientRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.entities.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientService extends BaseService {
    private final ClientRepository clientRepository;
    ClientService(DAOFactory daoFactory, BaseCoreConfiguration baseConfiguration) {
        super(daoFactory, baseConfiguration);
        clientRepository = daoFactory.getClientRepository();
    }

    public Optional<ClientJsonDecorator> getClient(Long clientId) {
        final Client clientById = clientRepository.get(clientId);
        if (clientById == null) {
            return Optional.empty();
        }
        return Optional.of(new ClientJsonDecorator(clientById));
    }

    public List<ClientJsonDecorator> getAllClients() {
        final List<Client> all = clientRepository.getAll();
        List<ClientJsonDecorator> decorators = new ArrayList<>();
        all.forEach(client -> decorators.add(new ClientJsonDecorator(client)));
        return decorators;
    }

    public Optional<ClientJsonDecorator> getClientByName(String clientName) {
        final Client clientByName = clientRepository.getClientByName(clientName);
        if (clientByName == null) {
            return Optional.empty();
        }
        return Optional.of(new ClientJsonDecorator(clientByName));
    }

    public Long createClient(ClientJsonDecorator client) {
        Client newClient = new Client(client.getClientName(), client.getClientEmail());
        return clientRepository.create(newClient);
    }

    public void updateClient(long clientId, ClientJsonDecorator updatedClient) {
        clientRepository.update(new Client(clientId, updatedClient.getClientName(), updatedClient.getClientEmail()));
    }

    public boolean deleteClient(long id) {
        final Client delete = clientRepository.delete(id);
        return delete != null;
    }
}
