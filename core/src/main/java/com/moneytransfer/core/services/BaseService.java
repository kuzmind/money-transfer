package com.moneytransfer.core.services;

import com.moneytransfer.core.config.BaseCoreConfiguration;
import com.moneytransfer.datastorage.dao.api.DAOFactory;

abstract class BaseService {
    private final DAOFactory daoFactory;
    private BaseCoreConfiguration baseConfiguration;

    public BaseService(DAOFactory daoFactory, BaseCoreConfiguration baseConfiguration) {
        this.daoFactory = daoFactory;
        this.baseConfiguration = baseConfiguration;
    }

    public DAOFactory getDaoFactory() {
        return daoFactory;
    }

    public BaseCoreConfiguration getBaseConfiguration() {
        return baseConfiguration;
    }
}
