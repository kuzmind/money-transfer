package com.moneytransfer.core.services;

import com.moneytransfer.core.config.BaseCoreConfiguration;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.utils.PropertyReader;

public enum ServiceFactory {
    INSTANCE;
    private AccountService accountService;
    private ClientService clientService;
    private TransactionService transactionService;

    ServiceFactory() {
        PropertyReader propertyReader = new PropertyReader();
        BaseCoreConfiguration configuration = new BaseCoreConfiguration(propertyReader);
        final String daoMode = configuration.getDAOMode();
        DAOFactory daoFactory;
        //place for extension
        switch (daoMode) {
            default: daoFactory = DAOFactory.getH2DAOFactory();
        }
        this.accountService = new AccountService(daoFactory, configuration);
        this.clientService = new ClientService(daoFactory, configuration);
        this.transactionService = new TransactionService(daoFactory, configuration);
    }

    public AccountService getAccountService() {
        return accountService;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }
}
