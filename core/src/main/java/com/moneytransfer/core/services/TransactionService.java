package com.moneytransfer.core.services;

import com.moneytransfer.core.config.BaseCoreConfiguration;
import com.moneytransfer.core.entities.AccountJsonDecorator;
import com.moneytransfer.core.entities.TransactionJsonDecorator;
import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.dao.api.TransactionRepository;
import com.moneytransfer.datastorage.entities.Transaction;
import com.moneytransfer.utils.MoneyExchanger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class to operate with transactions
 * We have to keep all transactions in immutable form that's why we have read-only operations and create
 */
public class TransactionService extends BaseService {
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;
    TransactionService(DAOFactory daoFactory, BaseCoreConfiguration baseConfiguration) {
        super(daoFactory, baseConfiguration);
        transactionRepository = daoFactory.getTransactionRepository();
        accountRepository = daoFactory.getAccountRepository();
    }

    public void performTransaction(long senderId, long receiverId, BigDecimal amount) {
        //todo implement if necessary
    }

    public void performTransaction(AccountJsonDecorator sender, AccountJsonDecorator receiver, BigDecimal amount) {
        final BigDecimal transactionCoefficient = MoneyExchanger.getTransactionCoefficient(amount, sender.getCurrencyCode(),
                receiver.getCurrencyCode());
        BigDecimal bankFee = new BigDecimal(0);
        if (!sender.getCurrencyCode().equals(receiver.getCurrencyCode())) {
            bankFee = MoneyExchanger.getBankFee().multiply(amount).setScale(2, RoundingMode.HALF_EVEN);
        }
        Transaction transaction = Transaction.createTransaction(sender.getAccountId(), receiver.getAccountId(), amount, sender.getCurrencyCode(),
                bankFee, transactionCoefficient);
        accountRepository.invokeTransaction(transaction);
        transactionRepository.create(transaction);
    }

    public void performTransaction(long senderId, long receiverId, BigDecimal amount, String currencyCode) {
        //todo implement if necessary
    }

    public Optional<TransactionJsonDecorator> getTransaction(long transactionId) {
        final Transaction transaction = transactionRepository.get(transactionId);
        if (transaction == null) {
            return Optional.empty();
        }
        return Optional.of(new TransactionJsonDecorator(transaction));
    }

    public List<TransactionJsonDecorator> getAllTransactions() {
        final List<Transaction> all = transactionRepository.getAll();
        List<TransactionJsonDecorator> decorators = new ArrayList<>();
        all.forEach(transaction -> decorators.add(new TransactionJsonDecorator(transaction)));
        return decorators;
    }
}
