package com.moneytransfer.core.services;

import com.moneytransfer.core.config.BaseCoreConfiguration;
import com.moneytransfer.core.entities.AccountJsonDecorator;
import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.entities.Account;
import com.moneytransfer.utils.BalanceFieldFilter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountService extends BaseService {
    private final AccountRepository accountRepository;
    AccountService(DAOFactory daoFactory, BaseCoreConfiguration baseConfiguration) {
        super(daoFactory, baseConfiguration);
        accountRepository = daoFactory.getAccountRepository();
    }

    public Long createAccount(String clientName, BigDecimal balance, String currencyCode) {
        return accountRepository.create(new Account(clientName, BalanceFieldFilter.scaleAndRound(balance), currencyCode));
    }

    public Long createAccount(AccountJsonDecorator accountJsonDecorator) {
        return accountRepository.create(new Account(accountJsonDecorator.getClientName(), accountJsonDecorator.getBalance(),
        accountJsonDecorator.getCurrencyCode()));
    }

    public void updateAccountBalance(Long accountId, BigDecimal delta) {
        final BigDecimal deltaAmount = BalanceFieldFilter.scaleAndRound(delta);
        accountRepository.updateAccountBalance(accountId, deltaAmount);
    }

    public Optional<AccountJsonDecorator> getAccount(Long accountId) {
        final Account account = accountRepository.get(accountId);
        if (account == null) {
            return Optional.empty();
        }
        return Optional.of(new AccountJsonDecorator(account));
    }

    public boolean deleteAccount(long accountId) {
        final Account delete = accountRepository.delete(accountId);
        return delete != null;
    }

    public List<AccountJsonDecorator> getAllAccounts() {
        final List<Account> all = accountRepository.getAll();
        List<AccountJsonDecorator> decorators = new ArrayList<>();
        all.forEach(account -> decorators.add(new AccountJsonDecorator(account)));
        return decorators;
    }
}
