package com.moneytransfer.core.services;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

public class AccountServiceTest {
    private static AccountService accountService;

    @BeforeClass
    public static void prepare() {
         accountService = ServiceFactory.INSTANCE.getAccountService();
    }

    @Test
    public void accountCreationTest() {
        String clientName = "accountCreationTest";
        BigDecimal balance = new BigDecimal(100);
        String currencyCode = "USD";
        final Long accountId = accountService.createAccount(clientName, balance, currencyCode);
        Assert.assertNotNull(accountId);
    }
}
