DROP TABLE IF EXISTS Client;

CREATE TABLE Client
(
    ClientId    LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    ClientName  VARCHAR(40)                     NOT NULL,
    ClientEmail VARCHAR(40)                     NOT NULL,
);

CREATE UNIQUE INDEX idx_clt on Client (ClientName, ClientEmail);

DROP TABLE IF EXISTS Transactions;

CREATE TABLE Transactions
(
    TransactionId           LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    SenderId                LONG                            NOT NULL,
    ReceiverId              LONG                            NOT NULL,
    Amount                  DECIMAL(19, 2),
    CurrencyCode            VARCHAR(3),
    TransferFee             DECIMAL(19, 2),
    TransferringCoefficient DECIMAL(19, 2)
);

DROP TABLE IF EXISTS Account;

CREATE TABLE Account
(
    AccountId    LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
    ClientName   VARCHAR(40),
    Balance      DECIMAL(19, 2),
    CurrencyCode VARCHAR(3)
);