package com.moneytransfer.datastorage.entities;

import java.math.BigDecimal;
import java.util.Objects;

public class Account {
    private long accountId;
    private String clientName;
    private BigDecimal balance;
    private String currencyCode;

    public Account(String clientName, BigDecimal balance, String currencyCode) {
        this.clientName = clientName;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public Account(long accountId, String clientName, BigDecimal balance, String currencyCode) {
        this.accountId = accountId;
        this.clientName = clientName;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getClientName() {
        return clientName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountId == account.accountId &&
                clientName.equals(account.clientName) &&
                balance.equals(account.balance) &&
                currencyCode.equals(account.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, clientName, balance, currencyCode);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", clientName='" + clientName + '\'' +
                ", balance=" + balance +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
