package com.moneytransfer.datastorage.entities;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Transaction class. Introduce transferring money operation.
 * transactionId - id of transaction
 * senderId - id of sender
 * receiverId - id of receiver
 * amount - amount of money which will be send from sender
 * currencyCode - currency of sender
 * transferFee - bank fee
 * transferringCoefficient - if sender and receiver have different currencies
 */
public class Transaction {
    private long transactionId;
    private long senderId;
    private long receiverId;
    private BigDecimal amount;
    private String currencyCode;
    private BigDecimal transferFee;
    private BigDecimal transferringCoefficient;

    public static Transaction createTransaction(long senderId, long receiverId, BigDecimal amount, String currencyCode, BigDecimal transferFee, BigDecimal transferringCoefficient) {
        return new Transaction(0, senderId, receiverId, amount, currencyCode, transferFee, transferringCoefficient);
    }

    public static Transaction createTransaction(long transactionId, long senderId, long receiverId, BigDecimal amount, String currencyCode, BigDecimal transferFee, BigDecimal transferringCoefficient) {
        return new Transaction(transactionId, senderId, receiverId, amount, currencyCode, transferFee, transferringCoefficient);
    }

    private Transaction(long transactionId, long senderId, long receiverId, BigDecimal amount, String currencyCode, BigDecimal transferFee, BigDecimal transferringCoefficient) {
        this.transactionId = transactionId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.transferFee = transferFee;
        this.transferringCoefficient = transferringCoefficient;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getTransferFee() {
        return transferFee;
    }

    public BigDecimal getTransferringCoefficient() {
        return transferringCoefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return transactionId == that.transactionId &&
                senderId == that.senderId &&
                receiverId == that.receiverId &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(currencyCode, that.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, senderId, receiverId, amount, currencyCode);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId=" + transactionId +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", amount=" + amount +
                ", currencyCode='" + currencyCode + '\'' +
                ", transferFee=" + transferFee +
                ", transferringCoefficient=" + transferringCoefficient +
                '}';
    }
}
