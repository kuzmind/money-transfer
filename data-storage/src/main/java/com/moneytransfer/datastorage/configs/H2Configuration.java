package com.moneytransfer.datastorage.configs;

import com.moneytransfer.utils.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class H2Configuration {
    private final String driver;
    private final String connectionUrl;
    private final String user;
    private final String password;
    private final String schemaFilePath;
    private final String H2_DRIVER = "h2_driver";
    private final String DEFAULT_H2_DRIVER = "org.h2.Driver";
    private final String H2_CONNECTION_URL = "h2_connection_url";
    private final String DEFAULT_CONNECTION_URL = "jdbc:h2:mem:moneyapp;DB_CLOSE_DELAY=-1";
    private final String H2_PASSWORD = "h2_password";
    private final String DEFAULT_H2_PASSWORD = "sa";
    private final String H2_USER = "h2_user";
    private final String DEFAULT_H2_USER = "sa";
    private final String H2_SCHEMA = "h2_schema";
    private final String DEFAULT_H2_SCHEMA = "h2Schema.sql";
    private Logger log = LoggerFactory.getLogger(H2Configuration.class);

    public H2Configuration(PropertyReader propertyReader) {
        driver = propertyReader.getStringProperty(H2_DRIVER, DEFAULT_H2_DRIVER);
        log.debug("Found property " + H2_DRIVER + " = " + driver);
        connectionUrl = propertyReader.getStringProperty(H2_CONNECTION_URL, DEFAULT_CONNECTION_URL);
        log.debug("Found property " + H2_CONNECTION_URL + " = " + connectionUrl);
        user = propertyReader.getStringProperty(H2_USER, DEFAULT_H2_USER);
        log.debug("Found property " + H2_USER + " = " + user);
        password = propertyReader.getStringProperty(H2_PASSWORD, DEFAULT_H2_PASSWORD);
        log.debug("Found property " + H2_PASSWORD + " = " + password);
        schemaFilePath = propertyReader.getStringProperty(H2_SCHEMA, DEFAULT_H2_SCHEMA);
        log.debug("Found property " + H2_SCHEMA + " = " + schemaFilePath);
    }

    public String getDriver() {
        return driver;
    }

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getSchemaFilePath() {
        return schemaFilePath;
    }
}
