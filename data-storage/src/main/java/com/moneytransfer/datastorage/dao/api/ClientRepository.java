package com.moneytransfer.datastorage.dao.api;

import com.moneytransfer.datastorage.entities.Client;

public interface ClientRepository extends BaseCRUDRepository<Client, Long> {

    Client getClientByName(String name);
}
