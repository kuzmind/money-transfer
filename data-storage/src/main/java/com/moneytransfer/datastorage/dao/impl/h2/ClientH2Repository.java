package com.moneytransfer.datastorage.dao.impl.h2;

import com.moneytransfer.datastorage.dao.api.ClientRepository;
import com.moneytransfer.datastorage.entities.Client;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientH2Repository extends BaseH2Repository implements ClientRepository {
    private static final Logger log = LoggerFactory.getLogger(ClientH2Repository.class);
    private final static String SQL_GET_CLIENT_BY_ID = "SELECT * FROM Client WHERE ClientId = ? ";
    private final static String SQL_GET_ALL_CLIENTS = "SELECT * FROM Client";
    private final static String SQL_GET_CLIENT_BY_NAME = "SELECT * FROM Client WHERE ClientName = ? ";
    private final static String SQL_INSERT_CLIENT = "INSERT INTO Client (ClientName, ClientEmail) VALUES (?, ?)";
    private final static String SQL_UPDATE_CLIENT = "UPDATE Client SET ClientName = ?, ClientEmail = ? WHERE ClientId = ? ";
    private final static String SQL_DELETE_CLIENT_BY_ID = "DELETE FROM Client WHERE ClientId = ? ";

    public ClientH2Repository(H2ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    @Override
    public Long create(Client object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_CLIENT);
            stmt.setString(1, object.getClientName());
            stmt.setString(2, object.getClientEmail());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                log.error("create(): Creating client failed, no rows affected." + object);
                throw new RuntimeException("Client Cannot be created");
            }
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                log.error("create(): Creating client failed, no ID obtained." + object);
                throw new RuntimeException("Client Cannot be created");
            }
        } catch (SQLException e) {
            log.error("Error Inserting Client :" + object);
            throw new RuntimeException("Error creating client data", e);
        } finally {
            DbUtils.closeQuietly(conn,stmt,generatedKeys);
        }
    }

    @Override
    public void update(Client object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = connectionProvider.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(SQL_UPDATE_CLIENT);
            stmt.setString(1, object.getClientName());
            stmt.setString(2, object.getClientEmail());
            stmt.setLong(3, object.getClientId());
            stmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            log.error("Error Updating Client  " + object);
            throw new RuntimeException("update(): Error updating client " + object, e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    @Override
    public Client delete(Long id) {
        final Client client = get(id);
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE_CLIENT_BY_ID);
            stmt.setLong(1, id);
            stmt.executeUpdate();
            conn.commit();
            return client;
        } catch (SQLException e) {
            log.error("Error Deleting Client :" + id);
            throw new RuntimeException("Error Deleting Client ID:"+ id, e);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
    }

    @Override
    public Client get(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Client u = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_CLIENT_BY_ID);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                u = retrieveClient(rs);
            }
            return u;
        } catch (SQLException e) {
            throw new RuntimeException("Error reading client data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    @Override
    public List<Client> getAll() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Client> clients = new ArrayList<>();
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ALL_CLIENTS);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Client u = retrieveClient(rs);
                clients.add(u);
            }
            return clients;
        } catch (SQLException e) {
            throw new RuntimeException("Error reading client data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    @Override
    public Client getClientByName(String name) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Client u = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_CLIENT_BY_NAME);
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            if (rs.next()) {
                u = retrieveClient(rs);
            }
            return u;
        } catch (SQLException e) {
            throw new RuntimeException("Error reading client data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    private Client retrieveClient(ResultSet rs) throws SQLException {
        Client client = new Client(rs.getLong("ClientId"), rs.getString("ClientName"), rs.getString("ClientEmail"));
        if (log.isDebugEnabled())
            log.debug("Retrieve Client By Id: " + client.getClientId());
        return client;
    }
}
