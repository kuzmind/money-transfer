package com.moneytransfer.datastorage.dao.api;

import com.moneytransfer.datastorage.configs.H2Configuration;
import com.moneytransfer.datastorage.dao.impl.h2.H2ConnectionProvider;
import com.moneytransfer.utils.PropertyReader;

public abstract class DAOFactory {
    private static final PropertyReader PROPERTY_READER = new PropertyReader();

    public abstract AccountRepository getAccountRepository();

    public abstract ClientRepository getClientRepository();

    public abstract TransactionRepository getTransactionRepository();

    public static H2DAOFactory getH2DAOFactory() {
        H2Configuration h2Config = new H2Configuration(PROPERTY_READER);
        H2ConnectionProvider h2ConnectionProvider = new H2ConnectionProvider(h2Config);
        return new H2DAOFactory(h2ConnectionProvider);
    }
}
