package com.moneytransfer.datastorage.dao.api;

import com.moneytransfer.datastorage.dao.impl.h2.AccountH2Repository;
import com.moneytransfer.datastorage.dao.impl.h2.ClientH2Repository;
import com.moneytransfer.datastorage.dao.impl.h2.H2ConnectionProvider;
import com.moneytransfer.datastorage.dao.impl.h2.TransactionH2Repository;
import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

public class H2DAOFactory extends DAOFactory {
    private final static Logger log = LoggerFactory.getLogger(H2DAOFactory.class);
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;
    private final TransactionRepository transactionRepository;

    public H2ConnectionProvider getConnectionProvider() {
        return connectionProvider;
    }

    private final H2ConnectionProvider connectionProvider;

    H2DAOFactory(H2ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
        accountRepository = new AccountH2Repository(connectionProvider);
        clientRepository = new ClientH2Repository(connectionProvider);
        transactionRepository = new TransactionH2Repository(connectionProvider);
    }

    @Override
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Override
    public ClientRepository getClientRepository() {
        return clientRepository;
    }

    @Override
    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

}
