package com.moneytransfer.datastorage.dao.api;

import java.util.List;

public interface BaseCRUDRepository<T, K> {

    K create(T object);

    void update(T object);

    T delete(K id);

    T get(K id);

    List<T> getAll();
}
