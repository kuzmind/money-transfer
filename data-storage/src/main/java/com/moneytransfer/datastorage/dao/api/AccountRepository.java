package com.moneytransfer.datastorage.dao.api;

import com.moneytransfer.datastorage.entities.Account;
import com.moneytransfer.datastorage.entities.Transaction;

import java.math.BigDecimal;

public interface AccountRepository extends BaseCRUDRepository<Account, Long> {
    void updateAccountBalance(Long accountId, BigDecimal deltaAmount);
    void invokeTransaction(Transaction transaction);
}
