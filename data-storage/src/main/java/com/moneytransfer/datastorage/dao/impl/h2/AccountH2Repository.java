package com.moneytransfer.datastorage.dao.impl.h2;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.entities.Account;
import com.moneytransfer.datastorage.entities.Transaction;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.moneytransfer.utils.BalanceFieldFilter.zeroAmount;

public class AccountH2Repository extends BaseH2Repository implements AccountRepository {
    private static final Logger log = LoggerFactory.getLogger(AccountH2Repository.class);
    private final static String SQL_GET_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ?";
    private final static String SQL_LOCK_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? FOR UPDATE";
    private final static String SQL_CREATE_ACC = "INSERT INTO Account (ClientName, Balance, CurrencyCode) VALUES (?, ?, ?)";
    private final static String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = ?";
    private final static String SQL_UPDATE_ACC = "UPDATE Account SET ClientName = ?, Balance = ?, CurrencyCode = ? WHERE AccountId = ?";
    private final static String SQL_GET_ALL_ACC = "SELECT * FROM Account";
    private final static String SQL_DELETE_ACC_BY_ID = "DELETE FROM Account WHERE AccountId = ?";

    public AccountH2Repository(H2ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    @Override
    public void invokeTransaction(Transaction transaction) {
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account sender = null;
        Account receiver = null;

        try {
            conn = connectionProvider.getConnection();
            conn.setAutoCommit(false);
            // lock the credit and debit account for writing:
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
            lockStmt.setLong(1, transaction.getSenderId());
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                sender = retrieveAccount(rs);
            }
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
            lockStmt.setLong(1, transaction.getReceiverId());
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                receiver = retrieveAccount(rs);
            }
            // check locking status
            if (sender == null || receiver == null) {
                throw new RuntimeException("Fail to lock both accounts for write");
            }

            // check enough fund in source account
            final BigDecimal amount = transaction.getAmount();
            final BigDecimal amountFromSender = amount.add(transaction.getTransferFee());
            BigDecimal fromAccountLeftOver = sender.getBalance().subtract(amountFromSender);
            if (fromAccountLeftOver.compareTo(zeroAmount) < 0) {
                throw new RuntimeException("Not enough Fund from source Account ");
            }
            // proceed with update
            updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
            updateStmt.setBigDecimal(1, fromAccountLeftOver);
            updateStmt.setLong(2, transaction.getSenderId());
            updateStmt.addBatch();
            final BigDecimal amountToReceiver = amount.multiply(transaction.getTransferringCoefficient());
            updateStmt.setBigDecimal(1, receiver.getBalance().add(amountToReceiver));
            updateStmt.setLong(2, transaction.getReceiverId());
            updateStmt.addBatch();
            int[] rowsUpdated = updateStmt.executeBatch();
            int result = rowsUpdated[0] + rowsUpdated[1];
            if (log.isDebugEnabled()) {
                log.debug("Number of rows updated for the transfer : " + result);
            }
            // If there is no error, commit the transaction
            conn.commit();
        } catch (SQLException se) {
            // rollback transaction if exception occurs
            log.error("transferAccountBalance(): User Transaction Failed, rollback initiated for: " + transaction,
                    se);
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                throw new RuntimeException("Fail to rollback transaction", re);
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
    }

    @Override
    public void updateAccountBalance(Long accountId, BigDecimal deltaAmount) {
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account targetAccount = null;
        try {
            conn = connectionProvider.getConnection();
            conn.setAutoCommit(false);
            // lock account for writing:
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
            lockStmt.setLong(1, accountId);
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                targetAccount = retrieveAccount(rs);
            }

            if (targetAccount == null) {
                throw new RuntimeException("updateAccountBalance(): fail to lock account : " + accountId);
            }
            // update account upon success locking
            BigDecimal balance = targetAccount.getBalance().add(deltaAmount);
            if (balance.compareTo(zeroAmount) < 0) {
                throw new RuntimeException("updateAccountBalance(): fail to update balance for account : " + accountId + " balance < 0");
            }

            updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
            updateStmt.setBigDecimal(1, balance);
            updateStmt.setLong(2, accountId);
            updateStmt.executeUpdate();
            conn.commit();
            if (log.isDebugEnabled())
                log.debug("New Balance after Update: " + balance);
        } catch (SQLException se) {
            // rollback transaction if exception occurs
            log.error("updateAccountBalance(): User Transaction Failed, rollback initiated for: " + accountId, se);
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                throw new RuntimeException("Fail to rollback transaction", re);
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
    }

    @Override
    public Long create(Account account) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_CREATE_ACC);
            stmt.setString(1, account.getClientName());
            stmt.setBigDecimal(2, account.getBalance());
            stmt.setString(3, account.getCurrencyCode());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                log.error("createAccount(): Creating account failed, no rows affected.");
                throw new RuntimeException("Account Cannot be created");
            }
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                log.error("Creating account failed, no ID obtained.");
                throw new RuntimeException("Account Cannot be created");
            }
        } catch (SQLException e) {
            log.error("Error Inserting Account  " + account);
            throw new RuntimeException("createAccount(): Error creating user account " + account, e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, generatedKeys);
        }
    }

    @Override
    public void update(Account account) {
        Connection conn = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        try {
            conn = connectionProvider.getConnection();
            conn.setAutoCommit(false);
            updateStmt = conn.prepareStatement(SQL_UPDATE_ACC);
            updateStmt.setString(1, account.getClientName());
            updateStmt.setBigDecimal(2, account.getBalance());
            updateStmt.setString(3, account.getCurrencyCode());
            updateStmt.setLong(4, account.getAccountId());
            updateStmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            log.error("Error Updating Account  " + account);
            throw new RuntimeException("update(): Error updating account " + account, e);
        } finally {
            DbUtils.closeQuietly(conn, updateStmt, rs);
        }
    }

    @Override
    public Account delete(Long id) {
        final Account account = get(id);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE_ACC_BY_ID);
            stmt.setLong(1, id);
            stmt.executeUpdate();
            conn.commit();
            return account;
        } catch (SQLException e) {
            throw new RuntimeException("deleteAccountById(): Error deleting user account Id " + id, e);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
    }

    @Override
    public Account get(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Account acc = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ACC_BY_ID);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                acc = retrieveAccount(rs);
            }
            return acc;
        } catch (SQLException e) {
            throw new RuntimeException("getAccountById(): Error reading account data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    @Override
    public List<Account> getAll() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Account> allAccounts = new ArrayList<>();
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ALL_ACC);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Account acc = retrieveAccount(rs);
                allAccounts.add(acc);
            }
            return allAccounts;
        } catch (SQLException e) {
            throw new RuntimeException("getAccountById(): Error reading account data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    private Account retrieveAccount(ResultSet rs) throws SQLException {
        Account acc = new Account(rs.getLong("AccountId"), rs.getString("ClientName"), rs.getBigDecimal("Balance"),
                rs.getString("CurrencyCode"));
        if (log.isDebugEnabled())
            log.debug("Retrieve Account By Id: " + acc.getAccountId());
        return acc;
    }
}
