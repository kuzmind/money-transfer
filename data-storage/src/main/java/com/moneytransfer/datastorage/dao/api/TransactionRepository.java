package com.moneytransfer.datastorage.dao.api;

import com.moneytransfer.datastorage.entities.Transaction;

public interface TransactionRepository extends BaseCRUDRepository<Transaction, Long> {
    @Override
    default void update(Transaction object) {
        throw new UnsupportedOperationException();
    }

    @Override
    default Transaction delete(Long id) {
        throw new UnsupportedOperationException();
    }
}
