package com.moneytransfer.datastorage.dao.impl.h2;

public abstract class BaseH2Repository {
    protected final H2ConnectionProvider connectionProvider;

    BaseH2Repository(H2ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }
}
