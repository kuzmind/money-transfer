package com.moneytransfer.datastorage.dao.impl.h2;

import com.moneytransfer.datastorage.configs.H2Configuration;
import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2ConnectionProvider {
    private static final Logger log = LoggerFactory.getLogger(H2ConnectionProvider.class);

    private final H2Configuration h2Config;

    public H2ConnectionProvider(H2Configuration h2Config) {
        this.h2Config = h2Config;
        initDataBaseDriver(h2Config);
        initDataBase();
    }

    private void initDataBaseDriver(H2Configuration h2Config) {
        boolean success = DbUtils.loadDriver(h2Config.getDriver());
        if (success) {
            log.debug("Successfully loaded H2 database driver");
        } else {
            log.error("Can't load H2 data base driver");
        }
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(h2Config.getConnectionUrl(), h2Config.getUser(), h2Config.getPassword());
    }

    public H2Configuration getH2Config() {
        return h2Config;
    }

    private void initDataBase() {
        final String schemaFilePath = h2Config.getSchemaFilePath();
        log.info("Initializing database from sql file - " + schemaFilePath);
        Connection conn = null;
        try {
            conn = getConnection();
            final URL resourceAsStream = this.getClass().getClassLoader().getResource(schemaFilePath);
            final File schemaFile = new File(resourceAsStream.toURI());
            RunScript.execute(conn, new FileReader(schemaFile));
        } catch (SQLException e) {
            log.error("Error creating data base schema: ", e);
            throw new RuntimeException(e);
        } catch (FileNotFoundException | URISyntaxException e) {
            log.error("Error finding schema script file ", e);
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }
}
