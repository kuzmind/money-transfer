package com.moneytransfer.datastorage.dao.impl.h2;

import com.moneytransfer.datastorage.dao.api.TransactionRepository;
import com.moneytransfer.datastorage.entities.Transaction;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TransactionH2Repository extends BaseH2Repository implements TransactionRepository {
    private static Logger log = LoggerFactory.getLogger(TransactionH2Repository.class);
    private final static String SQL_GET_TRANSACTION_BY_ID = "SELECT * FROM Transactions WHERE TransactionId = ? ";
    private final static String SQL_GET_ALL_TRANSACTIONS = "SELECT * FROM Transactions";
    private final static String SQL_INSERT_TRANSACTION = "INSERT INTO Transactions (SenderId, ReceiverId, Amount, CurrencyCode, TransferFee, TransferringCoefficient) VALUES (?, ?, ?, ?, ?, ?)";

    public TransactionH2Repository(H2ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    @Override
    public Long create(Transaction object) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_TRANSACTION);
            stmt.setLong(1, object.getSenderId());
            stmt.setLong(2, object.getReceiverId());
            stmt.setBigDecimal(3, object.getAmount());
            stmt.setString(4, object.getCurrencyCode());
            stmt.setBigDecimal(5, object.getTransferFee());
            stmt.setBigDecimal(6, object.getTransferringCoefficient());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                log.error("Creating transaction failed, no rows affected." + object);
                throw new RuntimeException("Transaction Cannot be created");
            }
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                log.error("Creating transaction failed, no ID obtained." + object);
                throw new RuntimeException("Transaction Cannot be created");
            }
        } catch (SQLException e) {
            log.error("Error Inserting Transaction :" + object);
            throw new RuntimeException("Error creating transaction data", e);
        } finally {
            DbUtils.closeQuietly(conn,stmt,generatedKeys);
        }
    }

    @Override
    public Transaction get(Long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Transaction u = null;
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_TRANSACTION_BY_ID);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                u = retrieveTransaction(rs);
            }
            return u;
        } catch (SQLException e) {
            throw new RuntimeException("Error reading client data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    private Transaction retrieveTransaction(ResultSet rs) throws SQLException {
        Transaction transaction = Transaction.createTransaction(rs.getLong("TransactionId"), rs.getLong("SenderId"),
                rs.getLong("ReceiverId"), rs.getBigDecimal("Amount"), rs.getString("CurrencyCode"),
                rs.getBigDecimal("TransferFee"), rs.getBigDecimal("TransferringCoefficient"));
        if (log.isDebugEnabled())
            log.debug("Retrieve Transaction By Id: " + transaction.getTransactionId());
        return transaction;
    }

    @Override
    public List<Transaction> getAll() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Transaction> transactions = new ArrayList<>();
        try {
            conn = connectionProvider.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ALL_TRANSACTIONS);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Transaction u = retrieveTransaction(rs);
                transactions.add(u);
            }
            return transactions;
        } catch (SQLException e) {
            throw new RuntimeException("Error reading client data", e);
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }
}
