package com.moneytransfer.datastorage.dao.impl;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.dao.api.H2DAOFactory;
import com.moneytransfer.datastorage.entities.Account;
import com.moneytransfer.datastorage.entities.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class AccountH2RepositoryTest {
    private static H2DAOFactory h2DAOFactory;
    private static AccountRepository accountRepository;
    @BeforeClass
    public static void prepare() {
        h2DAOFactory = DAOFactory.getH2DAOFactory();
        accountRepository = h2DAOFactory.getAccountRepository();
    }

    @Test
    public void initializationFromFileTest() {
        final List<Account> all = accountRepository.getAll();
        all.forEach(Assert::assertNotNull);
        Assert.assertEquals(6, all.size());
    }

    @Test
    public void creatAndGetTest() {
        final String userName = "test";
        final BigDecimal balance = new BigDecimal(150);
        final String currencyCode = "USD";
        Account account = new Account(userName, balance, currencyCode);
        final Long id = accountRepository.create(account);
        final Account accountFromDB = accountRepository.get(id);
        Assert.assertNotNull(accountFromDB);
        Assert.assertEquals(userName, accountFromDB.getClientName());
        Assert.assertEquals(balance.setScale(2), accountFromDB.getBalance());
        Assert.assertEquals(currencyCode, accountFromDB.getCurrencyCode());
    }

    @Test
    public void updateAccountTest() {
        final long firstAccountId = 1L;
        final Account account = accountRepository.get(firstAccountId);
        Assert.assertNotNull(account);
        Assert.assertEquals("test1", account.getClientName());
        Assert.assertEquals(new BigDecimal(150.5000).setScale(2),  account.getBalance());
        Assert.assertEquals("USD", account.getCurrencyCode());

        Account accountToUpdate = new Account(1L, "newTest", new BigDecimal(300), "RUB");
        accountRepository.update(accountToUpdate);

        final Account updatedAccount = accountRepository.get(firstAccountId);
        Assert.assertNotNull(updatedAccount);
        Assert.assertEquals("newTest", updatedAccount.getClientName());
        Assert.assertEquals(new BigDecimal(300).setScale(2),  updatedAccount.getBalance());
        Assert.assertEquals("RUB", updatedAccount.getCurrencyCode());
    }

    @Test
    public void updateBalanceTest() {
        final String updateBalanceTest = "updateBalanceTest";
        final String currencyCode = "EUR";
        Account newAccount = new Account(updateBalanceTest, new BigDecimal(1000), currencyCode);
        final Long id = accountRepository.create(newAccount);

        accountRepository.updateAccountBalance(id, new BigDecimal(300));
        Account account = accountRepository.get(id);
        Assert.assertNotNull(account);
        Assert.assertEquals(updateBalanceTest, account.getClientName());
        Assert.assertEquals(new BigDecimal(1300).setScale(2), account.getBalance());

        accountRepository.updateAccountBalance(id, new BigDecimal(-600));
        account = accountRepository.get(id);
        Assert.assertNotNull(account);
        Assert.assertEquals(updateBalanceTest, account.getClientName());
        Assert.assertEquals(new BigDecimal(700).setScale(2), account.getBalance());
    }

    @Test
    public void deleteAccountTest() {
        final String updateBalanceTest = "deleteAccountTest";
        final String currencyCode = "EUR";
        Account newAccount = new Account(updateBalanceTest, new BigDecimal(1000), currencyCode);
        final Long id = accountRepository.create(newAccount);
        accountRepository.delete(id);
        final Account account = accountRepository.get(id);
        Assert.assertNull(account);
    }

    @Test(expected = RuntimeException.class)
    public void updateAccountNegativeTest() {
        final String clientName = "updateAccountNegativeTest";
        final String currencyCode = "RUB";
        final BigDecimal accountBalance = new BigDecimal(1000);
        Account sender = new Account(clientName, accountBalance, currencyCode);
        final Long id = accountRepository.create(sender);
        accountRepository.updateAccountBalance(id, new BigDecimal(-2000));
    }

    @Test
    public void invokeTransactionWithoutFeesTest() {
        final String transactionTestUser1 = "invokeTransactionWithoutFeesTest1";
        final String currencyCode = "RUB";
        final BigDecimal senderBalanceBeforeTransaction = new BigDecimal(1000);
        Account sender = new Account(transactionTestUser1, senderBalanceBeforeTransaction, currencyCode);
        final Long senderId = accountRepository.create(sender);

        final String transactionTestUser2 = "invokeTransactionWithoutFeesTest2";
        final BigDecimal receiverBalanceBeforeTransaction = new BigDecimal(1000);
        Account receiver = new Account(transactionTestUser2, receiverBalanceBeforeTransaction, currencyCode);
        final Long receiverId = accountRepository.create(receiver);

        final BigDecimal amount = new BigDecimal(200);
        final BigDecimal bankFee = new BigDecimal(0);
        final BigDecimal coefficient = new BigDecimal(1);
        Transaction transaction = Transaction.createTransaction(senderId, receiverId,
                amount, currencyCode, bankFee, coefficient);
        accountRepository.invokeTransaction(transaction);

        final Account senderAfterTransaction = accountRepository.get(senderId);
        final Account receiverAfterTransaction = accountRepository.get(receiverId);
        final BigDecimal senderBalanceAfterTransaction = senderAfterTransaction.getBalance();
        final BigDecimal receiverBalanceAfterTransaction = receiverAfterTransaction.getBalance();
        Assert.assertEquals(senderBalanceBeforeTransaction.subtract(amount).setScale(2) ,senderBalanceAfterTransaction);
        Assert.assertEquals(receiverBalanceBeforeTransaction.add(amount).setScale(2) ,receiverBalanceAfterTransaction);
    }

    @Test
    public void invokeTransactionWithFeesTest() {
        final String transactionTestUser1 = "invokeTransactionWithFeesTest1";
        final String currencyCode = "RUB";
        final BigDecimal senderBalanceBeforeTransaction = new BigDecimal(1000);
        Account sender = new Account(transactionTestUser1, senderBalanceBeforeTransaction, currencyCode);
        final Long senderId = accountRepository.create(sender);

        final String transactionTestUser2 = "invokeTransactionWithFeesTest2";
        final BigDecimal receiverBalanceBeforeTransaction = new BigDecimal(1000);
        Account receiver = new Account(transactionTestUser2, receiverBalanceBeforeTransaction, currencyCode);
        final Long receiverId = accountRepository.create(receiver);

        final BigDecimal amount = new BigDecimal(200);
        final BigDecimal bankFee = new BigDecimal(10);
        final BigDecimal rubToPlnCoefficient = new BigDecimal(0.6).setScale(2, RoundingMode.HALF_EVEN);
        Transaction transaction = Transaction.createTransaction(senderId, receiverId,
                amount, currencyCode, bankFee, rubToPlnCoefficient);
        accountRepository.invokeTransaction(transaction);

        final Account senderAfterTransaction = accountRepository.get(senderId);
        final Account receiverAfterTransaction = accountRepository.get(receiverId);
        final BigDecimal senderBalanceAfterTransaction = senderAfterTransaction.getBalance();
        final BigDecimal receiverBalanceAfterTransaction = receiverAfterTransaction.getBalance();
        final BigDecimal expectedSenderBalance = senderBalanceBeforeTransaction.subtract(amount).subtract(bankFee).setScale(2);
        Assert.assertEquals(expectedSenderBalance,senderBalanceAfterTransaction);
        final BigDecimal multiply = amount.multiply(rubToPlnCoefficient);
        final BigDecimal expectedReceiverBalance = receiverBalanceBeforeTransaction.add(multiply).setScale(2);
        Assert.assertEquals(expectedReceiverBalance,receiverBalanceAfterTransaction);
    }
}
