package com.moneytransfer.datastorage.dao.impl.concurrent;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.dao.api.H2DAOFactory;
import com.moneytransfer.datastorage.entities.Account;
import org.apache.commons.dbutils.DbUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static com.moneytransfer.utils.BalanceFieldFilter.zeroAmount;

/**
 * Testcase to check concurrent access to Account
 * Logic is : Locker thread is performing update operation. After the Locker thread locks account row in database it
 * releases lock of ReentrantLock and waits until some other thread will wake him up. After the Locker thread has blocked
 * account row ModificationThread invokes update of the same account row.
 * Main thread is waiting until LockerThread and ModificationThread will finish all logic.
 *
 * Test map:
 * LockerThread -------->locks accountId=1-->wait()----------------------------------------wakes up and finishes transaction
 * ModificationThread---------------------------->updates accountId=1->receives Exception->
 * MainThread--------->wait------------------------------------------------------------------------------------------------>checks result
 *
 * Expected:
 * numberOfFinishedThreads = 1  -> Locker thread
 * numberOfFailedThreads = 1    -> Modification thread
 * new balance = 4150.50
 */
public class UpdateAlreadyLockedKey {
    private static final Logger log = LoggerFactory.getLogger(UpdateAlreadyLockedKey.class);
    protected H2DAOFactory daoFactory = DAOFactory.getH2DAOFactory();
    protected AtomicInteger numberOfFinishedThread = new AtomicInteger(0);
    protected AtomicInteger numberOfFailedThread = new AtomicInteger(0);
    protected AtomicBoolean lockerThreadWorkSignal = new AtomicBoolean(false);
    protected ReentrantLock lock;
    protected Condition mainThreadWaitCondition;
    protected Condition modificationCondition;
    protected Thread lockerThread;
    protected Thread modificationThread;

    @Before
    public void prepare() {
        lock = new ReentrantLock();
        mainThreadWaitCondition = lock.newCondition();
        modificationCondition = lock.newCondition();
        lockerThread = new LockerThread(lock, modificationCondition, mainThreadWaitCondition);
        lockerThread.setDaemon(true);
        modificationThread = new ModificationThread(lock, modificationCondition);
        modificationThread.setDaemon(true);
    }

    @Test
    public void concurrentModificationOfSharedKey() {
        log.info("Starting locker thread");
        lockerThread.start();
        log.info("Starting modification thread");
        modificationThread.start();
        log.info("Main thread is trying to get lock");
        lock.lock();
        try {
            log.info("Main thread is calling await");
            mainThreadWaitCondition.await();
            log.info("Main thread has been woken up");
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        checkFinalResult();
    }

    public void checkFinalResult() {
        AccountRepository accountRepository = daoFactory.getAccountRepository();
        Account account = accountRepository.get(1L);
        BigDecimal expectedBalance = new BigDecimal(4150.50).setScale(2, RoundingMode.HALF_EVEN);
        Assert.assertNotNull(account);
        Assert.assertEquals(expectedBalance, account.getBalance());
        Assert.assertEquals(1, numberOfFailedThread.get());
        Assert.assertEquals(1, numberOfFinishedThread.get());
    }

    /**
     * Thread which perform any operation on the key. For example it can be delete/update/performTransaction etc
     */
    public class ModificationThread extends Thread {

        private final ReentrantLock lock;
        private final Condition condition;

        ModificationThread(ReentrantLock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            lock.lock();
            log.info("Modification thread is holding lock");
            try {
                log.info("Modification thread is running...");
                //if this thread starts work before Locker thread it has to wait until Locker will finish its 1 phase
                while (!lockerThreadWorkSignal.get()) {
                    condition.await();
                }
                AccountRepository accountRepository = daoFactory.getAccountRepository();
                accountRepository.updateAccountBalance(1L, new BigDecimal(2000));
                log.info("Modification thread is calling signal for LockerThread");
            } catch (Exception e){
                e.printStackTrace();
                log.error(e.getMessage());
            } finally {
                numberOfFailedThread.incrementAndGet();
                condition.signal();
                lock.unlock();
            }
        }
    }

    /**
     * Thread which fully imitates update operation of account @see {@link com.moneytransfer.datastorage.dao.impl.h2.AccountH2Repository}
     */
    public class LockerThread extends Thread {

        private final ReentrantLock lock;
        private final Condition condition;
        private Condition mainCondition;

        LockerThread(ReentrantLock lock, Condition condition, Condition mainCondition) {
            this.lock = lock;
            this.condition = condition;
            this.mainCondition = mainCondition;
        }

        @Override
        public void run() {
            lock.lock();
            log.info("Locker thread is holding lock");
            log.info("Locker thread is running...");
            lockerThreadWorkSignal.set(true);
            final String SQL_LOCK_ACC = "SELECT * FROM Account WHERE AccountId = 1 FOR UPDATE";
            final String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = 1";
            Connection connection = null;
            PreparedStatement updateStmt = null;
            PreparedStatement lockStmt = null;
            ResultSet rs = null;
            Account accountToUpdate = null;
            BigDecimal deltaAmount = new BigDecimal(4000).setScale(2, RoundingMode.HALF_EVEN);
            try {
                connection = daoFactory.getConnectionProvider().getConnection();
                connection.setAutoCommit(false);
                // lock account for writing:
                lockStmt = connection.prepareStatement(SQL_LOCK_ACC);
                rs = lockStmt.executeQuery();
                if (rs.next()) {
                    accountToUpdate = new Account(rs.getLong("AccountId"), rs.getString("ClientName"),
                            rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
                    if (log.isDebugEnabled())
                        log.debug("Locked Account: " + accountToUpdate);
                }

                if (accountToUpdate == null) {
                    throw new RuntimeException("Locking error during test, SQL = " + SQL_LOCK_ACC);
                }
                log.info("Locker thread is calling condition await()");
                //release lock and wait
                condition.await();
                log.info("Locker thread has been woken up");
                log.info("Locker thread is continue running ...");
                // update account upon success locking
                BigDecimal balance = accountToUpdate.getBalance().add(deltaAmount);
                if (balance.compareTo(zeroAmount) < 0) {
                    throw new RuntimeException("updateAccountBalance(): fail to update balance for account : " + accountToUpdate.getAccountId() + " balance < 0");
                }

                updateStmt = connection.prepareStatement(SQL_UPDATE_ACC_BALANCE);
                updateStmt.setBigDecimal(1, balance);
                updateStmt.executeUpdate();
                connection.commit();
                numberOfFinishedThread.incrementAndGet();
                mainCondition.signal();
            } catch (InterruptedException | SQLException e) {
                log.error(e.getMessage());
                e.printStackTrace();
                try {
                    if (connection != null)
                        connection.rollback();
                } catch (SQLException re) {
                    throw new RuntimeException("Fail to rollback transaction", re);
                }
            } finally {
                DbUtils.closeQuietly(connection);
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(lockStmt);
                DbUtils.closeQuietly(updateStmt);
                lock.unlock();
            }
        }
    }
}
