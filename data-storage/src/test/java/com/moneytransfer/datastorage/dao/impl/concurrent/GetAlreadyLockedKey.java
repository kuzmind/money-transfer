package com.moneytransfer.datastorage.dao.impl.concurrent;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.entities.Account;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Try to receive locked account row by GET operation. GET is unblocked operation
 * Expected:
 * numberOfFinishedThread = 2
 */
public class GetAlreadyLockedKey extends UpdateAlreadyLockedKey {
    private static final Logger log = LoggerFactory.getLogger(GetAlreadyLockedKey.class);
    @Override
    public void prepare() {
        lock = new ReentrantLock();
        mainThreadWaitCondition = lock.newCondition();
        modificationCondition = lock.newCondition();
        lockerThread = new LockerThread(lock, modificationCondition, mainThreadWaitCondition);
        lockerThread.setDaemon(true);
        modificationThread = new GetLockedKeyThread(lock, modificationCondition);
        modificationThread.setDaemon(true);
    }

    @Override
    public void checkFinalResult() {
        AccountRepository accountRepository = daoFactory.getAccountRepository();
        Account account = accountRepository.get(1L);
        BigDecimal expectedBalance = new BigDecimal(4150.50).setScale(2, RoundingMode.HALF_EVEN);
        Assert.assertNotNull(account);
        Assert.assertEquals(expectedBalance, account.getBalance());
        Assert.assertEquals(0, numberOfFailedThread.get());
        Assert.assertEquals(2, numberOfFinishedThread.get());
    }

    /**
     * Thread which perform get account where accountId=1 operation
     */
    public class GetLockedKeyThread extends Thread {

        private final ReentrantLock lock;
        private final Condition condition;

        GetLockedKeyThread(ReentrantLock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            lock.lock();
            log.info("Modification thread is holding lock");
            try {
                log.info("Modification thread is running...");
                while (!lockerThreadWorkSignal.get()) {
                    condition.await();
                }
                AccountRepository accountRepository = daoFactory.getAccountRepository();
                accountRepository.get(1L);
                numberOfFinishedThread.incrementAndGet();
                log.info("Modification thread is calling signal for LockerThread");
            } catch (Exception e){
                e.printStackTrace();
                log.error(e.getMessage());
            } finally {
                condition.signal();
                lock.unlock();
            }
        }
    }
}
