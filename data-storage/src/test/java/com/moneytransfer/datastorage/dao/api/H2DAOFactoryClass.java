package com.moneytransfer.datastorage.dao.api;

import com.moneytransfer.datastorage.dao.impl.h2.H2ConnectionProvider;
import org.junit.Assert;
import org.junit.Test;

public class H2DAOFactoryClass {

    @Test
    public void creationTest() {
        final H2DAOFactory h2DAOFactory = DAOFactory.getH2DAOFactory();
        Assert.assertNotNull(h2DAOFactory);
        final AccountRepository accountRepository = h2DAOFactory.getAccountRepository();
        Assert.assertNotNull(accountRepository);
        final H2ConnectionProvider connectionProvider = h2DAOFactory.getConnectionProvider();
        Assert.assertNotNull(connectionProvider);
    }
}
