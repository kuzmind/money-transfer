package com.moneytransfer.datastorage.dao.impl;

import com.moneytransfer.datastorage.dao.api.ClientRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.dao.api.H2DAOFactory;
import com.moneytransfer.datastorage.entities.Client;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class ClientH2RepositoryTest {
    private static ClientRepository h2Repository;
    private static H2DAOFactory h2DAOFactory;

    @BeforeClass
    public static void prepare() {
        h2DAOFactory = DAOFactory.getH2DAOFactory();
        h2Repository = h2DAOFactory.getClientRepository();
    }

    @Test
    public void initializationFromFileTest() {
        final List<Client> all = h2Repository.getAll();
        Assert.assertEquals(3, all.size());
        all.forEach(Assert::assertNotNull);
    }

    @Test
    public void createClientTest() {
        String clientName = "createClientTest";
        String clientEmail = "createClientTest@gmail.com";
        Client client = new Client(clientName, clientEmail);
        final Long id = h2Repository.create(client);
        final Client clientFromDB = h2Repository.get(id);
        Assert.assertNotNull(clientFromDB);
        Assert.assertEquals(clientEmail, clientFromDB.getClientEmail());
        Assert.assertEquals(clientName, clientFromDB.getClientName());
    }

    @Test
    public void updateClientTest() {
        String clientName = "updateClientTest";
        String clientEmail = "updateClientTest@gmail.com";
        Client client = new Client(clientName, clientEmail);
        final Long id = h2Repository.create(client);
        String newClientName = clientName + "New";
        String newClientEmail = clientEmail + "New";
        Client newClient = new Client(id, newClientName, newClientEmail);
        h2Repository.update(newClient);
        final Client updatedClient = h2Repository.get(id);
        Assert.assertNotNull(updatedClient);
        Assert.assertEquals(newClientEmail, updatedClient.getClientEmail());
        Assert.assertEquals(newClientName, updatedClient.getClientName());
    }

    @Test
    public void deleteClientTest() {
        String clientName = "deleteClientTest";
        String clientEmail = "deleteClientTest@gmail.com";
        Client client = new Client(clientName, clientEmail);
        final Long id = h2Repository.create(client);
        h2Repository.delete(id);
        final Client deletedClient = h2Repository.get(id);
        Assert.assertNull(deletedClient);
    }

    @Test
    public void getClientByName() {
        String clientName = "getClientByNameTest";
        String clientEmail = "getClientByNameTest@gmail.com";
        Client client = new Client(clientName, clientEmail);
        h2Repository.create(client);
        final Client clientByName = h2Repository.getClientByName(clientName);
        Assert.assertNotNull(clientByName);
        Assert.assertEquals(clientName, clientByName.getClientName());
        Assert.assertEquals(clientEmail, clientByName.getClientEmail());
    }
}
