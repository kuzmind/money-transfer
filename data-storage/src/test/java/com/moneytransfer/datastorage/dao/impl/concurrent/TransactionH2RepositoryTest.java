package com.moneytransfer.datastorage.dao.impl.concurrent;

import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.dao.api.H2DAOFactory;
import com.moneytransfer.datastorage.dao.api.TransactionRepository;
import com.moneytransfer.datastorage.entities.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

public class TransactionH2RepositoryTest {
    private static H2DAOFactory h2DAOFactory;
    private static TransactionRepository transactionRepository;

    @BeforeClass
    public static void prepare() {
        h2DAOFactory = DAOFactory.getH2DAOFactory();
        transactionRepository = h2DAOFactory.getTransactionRepository();
    }

    @Test
    public void createTransactionTest() {
        final BigDecimal amount = new BigDecimal(200).setScale(2);
        long idFrom = 1;
        long idTo = 2;
        String currencyCode = "USD";
        final BigDecimal bankFee = new BigDecimal(0).setScale(2);
        final BigDecimal coefficient = new BigDecimal(1).setScale(2);
        Transaction transaction = Transaction.createTransaction(idFrom, idTo, amount, currencyCode, bankFee, coefficient);
        final Long transactionId = transactionRepository.create(transaction);
        final Transaction transaction1 = transactionRepository.get(transactionId);
        Assert.assertNotNull(transaction1);
        Assert.assertEquals(currencyCode, transaction1.getCurrencyCode());
        Assert.assertEquals(amount, transaction1.getAmount());
        Assert.assertEquals(bankFee, transaction1.getTransferFee());
        Assert.assertEquals(coefficient, transaction1.getTransferringCoefficient());
    }
}
