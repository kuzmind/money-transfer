package com.moneytransfer.datastorage.dao.impl.concurrent;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.dao.api.DAOFactory;
import com.moneytransfer.datastorage.dao.api.H2DAOFactory;
import com.moneytransfer.datastorage.entities.Account;
import com.moneytransfer.datastorage.entities.Transaction;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Test scenario:
 * Simultaneously transfer money from account1 to account2 + update account1; Meanwhile count the number of success transactions
 * and success updates.
 * test data:
 * account1 - (accountId = 5, clientName = test1, balance = 500.00, currencyCode = GBP)
 * account2 - (accountId = 6, clientName = test2, balance = 500.00, currencyCode = GBP)
 * transaction balance = 100
 * updateAmount = 50
 * number of threads for transaction = 20
 * number of threads for update = 20
 * Expected:
 * 1) The result balance of account2 should be = numberOfSuccessTransactions * transactionAmount
 * 2) If the number of success transactions > 5 (5 because initial balance 500 and transaction amount is 100) than
 *    (numberOfSuccessUpdates * 50) + 500 = resultBalanceOFAccount1 + resultBalanceOFAccount2
 * 3) numberOfSuccessUpdates * 50 + numberOfSuccessTransactions * 100 = resultBalanceOFAccount1 + resultBalanceOFAccount2
 *    - initAmountOfReceiver
 * 4) numberOfSuccessUpdates = (numberOfSuccessTransactions - 5) * 2 + (resultBalanceOFAccount1 / 50)
 */
public class MultiThreadedAccountModificationTest {
    private final static Logger log = LoggerFactory.getLogger(MultiThreadedAccountModificationTest.class);
    private final int THREAD_COUNT = 10;
    private final long senderId = 5L;
    private final long receiverId = 6L;
    private final String currencyCode = "GBP";
    private final BigDecimal senderInitialBalance = new BigDecimal(500);
    private final BigDecimal receiverInitialBalance = new BigDecimal(500);
    private final BigDecimal transactionAmount = new BigDecimal(100).setScale(2, RoundingMode.HALF_EVEN);
    private final BigDecimal updateAmount = new BigDecimal(50).setScale(2, RoundingMode.HALF_EVEN);
    private final AtomicInteger numberOfSuccessTransactions = new AtomicInteger(0);
    private final AtomicInteger numberOfSuccessUpdates = new AtomicInteger(0);
    private final CountDownLatch countDownLatch = new CountDownLatch(THREAD_COUNT);
    ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(THREAD_COUNT);

    @Test
    public void multiThreadAccountUpdate() throws InterruptedException {
        H2DAOFactory h2DAOFactory = DAOFactory.getH2DAOFactory();
        AccountRepository accountRepository = h2DAOFactory.getAccountRepository();
        for (int i = 0; i < THREAD_COUNT; i++) {
            if (i % 2 == 0) {
                threadPoolExecutor.submit(new TransactionThread(accountRepository));
            } else {
                threadPoolExecutor.submit(new UpdateThread(accountRepository));
            }
        }
        countDownLatch.await();
        Account receiver = accountRepository.get(receiverId);
        BigDecimal expectedReceiverBalance = transactionAmount.multiply(new BigDecimal(numberOfSuccessTransactions.get())).add(receiverInitialBalance);
        //first condition
        Assert.assertEquals(expectedReceiverBalance, receiver.getBalance());
        Account sender = accountRepository.get(senderId);
        if (numberOfSuccessTransactions.get() > 5) {
            BigDecimal expectedAllAmount = updateAmount.multiply(new BigDecimal(numberOfSuccessUpdates.get())).
                    add(senderInitialBalance).add(receiverInitialBalance);
            //second condition
            Assert.assertEquals(expectedAllAmount, receiver.getBalance().add(sender.getBalance()));
        }
        //third condition
        final BigDecimal resultSumFromUpdates = updateAmount.multiply(new BigDecimal(numberOfSuccessUpdates.get()));
        final BigDecimal resultSumFromTransactions = transactionAmount.multiply(new BigDecimal(numberOfSuccessTransactions.get()));
        Assert.assertEquals(resultSumFromTransactions.add(resultSumFromUpdates),
                receiver.getBalance().add(sender.getBalance()).subtract(receiverInitialBalance));
        //fourth condition
        final BigDecimal numberOfTransactionsExtra = new BigDecimal(numberOfSuccessTransactions.get()).subtract(new BigDecimal(5));
        final BigDecimal divide = sender.getBalance().divide(updateAmount, RoundingMode.HALF_EVEN);
        final BigDecimal numberOfUpdates = numberOfTransactionsExtra.multiply(new BigDecimal(2)).add(divide);
        Assert.assertEquals(new BigDecimal(numberOfSuccessUpdates.get()).setScale(2, RoundingMode.HALF_EVEN), numberOfUpdates);
    }

    private class TransactionThread extends Thread {

        private AccountRepository accountRepository;

        TransactionThread(AccountRepository accountRepository) {
            this.accountRepository = accountRepository;
        }

        @Override
        public void run() {
            BigDecimal bigDecimalZero = new BigDecimal(0);
            BigDecimal bigDecimalOne = new BigDecimal(1);
            Transaction transaction = Transaction.createTransaction(senderId, receiverId, transactionAmount,
                    currencyCode, bigDecimalZero, bigDecimalOne);
            try {
                log.info("Invoking transaction");
                accountRepository.invokeTransaction(transaction);
                numberOfSuccessTransactions.incrementAndGet();
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }
    }

    private class UpdateThread extends Thread {

        private AccountRepository accountRepository;

        UpdateThread(AccountRepository accountRepository) {
            this.accountRepository = accountRepository;
        }

        @Override
        public void run() {
            try {
                log.info("Updating balance");
                accountRepository.updateAccountBalance(senderId, updateAmount);
                numberOfSuccessUpdates.incrementAndGet();
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }
    }
}
