package com.moneytransfer.datastorage.dao.impl.concurrent;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import com.moneytransfer.datastorage.entities.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The same logic as {@link UpdateAlreadyLockedKey}
 */
public class InvokeTransactionWithLockedKey extends UpdateAlreadyLockedKey {
    private static final Logger log = LoggerFactory.getLogger(InvokeTransactionWithLockedKey.class);

    @Override
    public void prepare() {
        lock = new ReentrantLock();
        mainThreadWaitCondition = lock.newCondition();
        modificationCondition = lock.newCondition();
        lockerThread = new LockerThread(lock, modificationCondition, mainThreadWaitCondition);
        lockerThread.setDaemon(true);
        modificationThread = new TransactionWithLockedKeyThread(lock, modificationCondition);
        modificationThread.setDaemon(true);
    }

    /**
     * Thread which perform delete account where accountId=1 operation
     */
    public class TransactionWithLockedKeyThread extends Thread {

        private final ReentrantLock lock;
        private final Condition condition;

        TransactionWithLockedKeyThread(ReentrantLock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            lock.lock();
            log.info("Modification thread is holding lock");
            try {
                log.info("Modification thread is running...");
                while (!lockerThreadWorkSignal.get()) {
                    condition.await();
                }
                AccountRepository accountRepository = daoFactory.getAccountRepository();
                Transaction transaction = Transaction.createTransaction(1L, 2L, new BigDecimal(10000), "USD", new BigDecimal(0), new BigDecimal(0));
                accountRepository.invokeTransaction(transaction);
                log.info("Modification thread is calling signal for LockerThread");
            } catch (Exception e){
                e.printStackTrace();
                log.error(e.getMessage());
            } finally {
                numberOfFailedThread.incrementAndGet();
                condition.signal();
                lock.unlock();
            }
        }
    }
}
