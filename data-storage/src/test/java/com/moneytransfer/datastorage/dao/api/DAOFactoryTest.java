package com.moneytransfer.datastorage.dao.api;

import org.junit.Assert;
import org.junit.Test;

public class DAOFactoryTest {

    @Test
    public void creationTest() {
        final H2DAOFactory h2DAOFactory = DAOFactory.getH2DAOFactory();
        Assert.assertNotNull(h2DAOFactory);
    }
}
