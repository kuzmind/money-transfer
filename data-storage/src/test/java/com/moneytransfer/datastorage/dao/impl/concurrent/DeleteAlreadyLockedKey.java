package com.moneytransfer.datastorage.dao.impl.concurrent;

import com.moneytransfer.datastorage.dao.api.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The same logic as @see {@link UpdateAlreadyLockedKey}
 */
public class DeleteAlreadyLockedKey extends UpdateAlreadyLockedKey {
    private static final Logger log = LoggerFactory.getLogger(DeleteAlreadyLockedKey.class);

    @Override
    public void prepare() {
        lock = new ReentrantLock();
        mainThreadWaitCondition = lock.newCondition();
        modificationCondition = lock.newCondition();
        lockerThread = new LockerThread(lock, modificationCondition, mainThreadWaitCondition);
        lockerThread.setDaemon(true);
        modificationThread = new DeleteLockedKeyThread(lock, modificationCondition);
        modificationThread.setDaemon(true);
    }

    /**
     * Thread which performs delete account operation where accountId=1
     */
    public class DeleteLockedKeyThread extends Thread {

        private final ReentrantLock lock;
        private final Condition condition;

        DeleteLockedKeyThread(ReentrantLock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            lock.lock();
            log.info("Modification thread is holding lock");
            try {
                log.info("Modification thread is running...");
                while (!lockerThreadWorkSignal.get()) {
                    condition.await();
                }
                AccountRepository accountRepository = daoFactory.getAccountRepository();
                accountRepository.delete(1L);
                log.info("Modification thread is calling signal for LockerThread");
            } catch (Exception e){
                e.printStackTrace();
                log.error(e.getMessage());
            } finally {
                numberOfFailedThread.incrementAndGet();
                condition.signal();
                lock.unlock();
            }
        }
    }
}
