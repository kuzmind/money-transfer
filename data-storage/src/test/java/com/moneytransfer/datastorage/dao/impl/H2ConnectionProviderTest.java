package com.moneytransfer.datastorage.dao.impl;

import com.moneytransfer.datastorage.configs.H2Configuration;
import com.moneytransfer.datastorage.dao.impl.h2.H2ConnectionProvider;
import com.moneytransfer.utils.PropertyReader;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class H2ConnectionProviderTest {

    @Test
    public void connectionProviderCheckConnectTest() {
        H2Configuration h2Config = new H2Configuration(new PropertyReader());
        H2ConnectionProvider h2ConnectionProvider = new H2ConnectionProvider(h2Config);
        try {
            final Connection connection = h2ConnectionProvider.getConnection();
            Assert.assertNotNull(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
