package com.moneytransfer.datastorage.configs;

import com.moneytransfer.utils.PropertyReader;
import org.junit.Assert;
import org.junit.Test;

public class H2ConfigurationTest {

    @Test
    public void test() {
        PropertyReader propertyReader = new PropertyReader();
        H2Configuration h2Config = new H2Configuration(propertyReader);
        final String connectionUrl = h2Config.getConnectionUrl();
        final String driver = h2Config.getDriver();
        final String password = h2Config.getPassword();
        final String user = h2Config.getUser();
        Assert.assertEquals("sa", user);
        Assert.assertEquals("sa", password);
        Assert.assertEquals("org.h2.Driver", driver);
        Assert.assertEquals("jdbc:h2:mem:moneyapp;DB_CLOSE_DELAY=-1", connectionUrl);
    }
}
